1. Download Miniconda3: https://conda.io/projects/conda/en/latest/user-guide/install/index.html  
2. This link helps you getting started with conda commands: https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html  
3. Open a terminal (inside your IDE) and go into the projects folder. Hit the command: 'conda env create -f environment.yml'
4. Once the environment is created, hit: 'conda activate alvuc'
5. Change the directory name "cuvla" into "alvuc"
6. Run 'python experiment_cli/experiment.py' 
7. Adjust params.yaml with parameters as you wish, for example to decide on which pretrained model architecture, your model is training
