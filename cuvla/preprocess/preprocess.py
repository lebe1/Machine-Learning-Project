# %%
import random
from pathlib import Path
from typing import NamedTuple

import numpy as np
import pandas as pd
import yaml
from loguru import logger
from PIL import Image
from PIL.Image import Image as PIL_Image  # only used in order for static typing to work

import alvuc.gpu_utils
from alvuc.functions import config
from alvuc.util import AlvucDataGenerator

# FIXME cv2 requires conda package opencv


alvuc.gpu_utils.mask_unused_gpus(count_gpus_unmasked=1, minimum_available_ram=9000)


class ImageDimensions(NamedTuple):
    """
    Create NamedTuple to be used for saving image dimensions
    """

    width: int
    height: int


def apply_resize_method_to_img(
    raw_img: PIL_Image, RESIZE_METHOD: str, OUTPUT_IMAGE_SIZE: ImageDimensions
) -> PIL_Image:
    """
    This function is responsible for handling the different resize methods and to calculate
    the the target size of the image, respecting the original aspect ratio.
    See examples of the different resize methods:
    https://git.rz.uni-augsburg.de/swtpvs-lifedata/alvuc/-/issues/176

    Args:
        raw_img (PIL_Image): PIL Image object containing the picture to be resized.
        RESIZE_METHOD (str): Method to be used for image resizing. This method is only taking effect
            only taking effect if the aspect ratio of the raw_img is different to the one
            of the OUTPUT_IMAGE_SIZE.
            Available options: "Crop", "Fit", "None"
        OUTPUT_IMAGE_SIZE (ImageDimensions): NamedTuple containing the desired image output size,
            derived from params.yaml.
            Example: (width = 300, height = 200)

    Returns:
        PIL_Image: PIL Image object with the resized picture respecting RESIZE_METHOD and desired
            OUTPUT_IMAGE_SIZE.
    """

    raw_img_size = ImageDimensions(width=raw_img.size[0], height=raw_img.size[1])

    # Calculation of the resizing factor applied to the image
    # "fit": fit the longer side of raw_img into OUTPUT_IMAGE_SIZE (show black borders)
    # "crop": fit the smaller side of raw_img into OUTPUT_IMAGE_SIZE (cut off remaining parts of the longer side)
    if RESIZE_METHOD.lower() == "fit":
        proportion = max(OUTPUT_IMAGE_SIZE.width, OUTPUT_IMAGE_SIZE.height) / max(raw_img_size)
    elif RESIZE_METHOD.lower() == "crop":
        proportion = max(OUTPUT_IMAGE_SIZE.width, OUTPUT_IMAGE_SIZE.height) / min(raw_img_size)

    # Calculate new image dimensions
    target_size = ImageDimensions(
        width=int(proportion * raw_img_size.width), height=int(proportion * raw_img_size.height)
    )

    # Resize raw image
    raw_img_resized = raw_img.resize(target_size, Image.ANTIALIAS)

    # Create blank output image with specified dimensions in params.yaml
    output_image = Image.new("RGB", OUTPUT_IMAGE_SIZE)
    # Paste in raw_img_resized into the "middle" of the output image
    output_image.paste(
        raw_img_resized,
        (
            (OUTPUT_IMAGE_SIZE.width - target_size.width) // 2,
            (OUTPUT_IMAGE_SIZE.height - target_size.height) // 2,
        ),
    )

    return output_image


def preprocess_image(
    raw_img_path: Path, RESIZE_METHOD: str, OUTPUT_IMAGE_SIZE: ImageDimensions
) -> PIL_Image:
    """
    This function is responsible for preprocessing an image. Every image path passed to this function
    will be resized to the dimensions set in params.yaml and saved in the PREPROCESSED_PATH folder.

    Args:
        raw_img_path (Path): Path object containing the path of the image to be preprocessed
        RESIZE_METHOD (str): Method to be used if the image is resized to a different aspect ratio
            available options: "crop", "fit", "none"
        OUTPUT_IMAGE_SIZE (ImageDimensions): NamedTuple containing the desired image output size.
            Example: (width = 300, height = 200)

    Returns:
        PIL_Image: PIL Image object with the resized picture to be saved.
    """

    raw_img = Image.open(raw_img_path)

    if RESIZE_METHOD.lower() in ("crop", "fit"):
        # If aspect ratios of input and output images are different, then call special resize function
        output_image = apply_resize_method_to_img(raw_img, RESIZE_METHOD, OUTPUT_IMAGE_SIZE)
    else:
        output_image = raw_img.resize(OUTPUT_IMAGE_SIZE, Image.ANTIALIAS)

    return output_image


def upsample_dataframe(
    metadata_df: pd.DataFrame, num_samples: int, y_col: str, width: int, height: int
):
    # TODO write proper text for function
    """
    Train_test_split_static picks {test_size} samples per class into the test set
    Remaining records will be put in the train set.

    ATTENTION: A test_size setting of 10 does not necessarily guarantee that your test set is evenly spread
    with exactly 10 samples per class because it is possible that a group_col might contribute more than one sample.

    Args:
        metadata_df (dataframe): Dataframe to be split into train and test sets
        random_state (int): The random_state defines the pandas sample random state
        y_col (str): Denotes the column name of your target class to be predicted
        group_col (str): Denotes the column name containing the groups which will either be split into test or train set, but not both
                            Example: Three samples share the same group_col value. It cannot happen that, for example,
                            2 of them go into train and 1 into test. All 3 will either be part of train or test.
        test_size (int): The sample count of each class for the test set (see "ATTENTION" remark above why count could be more)

    Returns:
        Tuple[pd.DataFrame]: train DataFrame, test DataFrame

    """
    if len(metadata_df) == 0:
        logger.warning("No data to downsample. Returning empty dataframes.")
        return metadata_df.copy()

    # TODO this should actually happen in the config.py
    clazzes = [clazz.strip() for clazz in dvc_paths.configs["labels"].split(",")]

    # Build the path to the preprocessed images. DataGenerator later expects strings => cast to string
    metadata_df["path_preprocessed"] = metadata_df.apply(
        lambda row: str(dvc_paths.preprocessed_path / (row["image_id"] + ".jpg")), axis=1
    )
    # Get frequency counts for your target variables and save dictionary
    dxs = metadata_df[y_col].value_counts()
    dxs = dxs.to_dict()

    DATA_RAW = dvc_paths.raw_data_path
    DATA_PREPROCESSED = dvc_paths.preprocessed_path
    DATA_LABEL_STATE = dvc_paths.label_state_path

    # raw_img_paths = list(DATA_RAW.glob(r"**/*.jpg"))
    # raw_img_paths.sort()
    # metadata_df.sort_values(by=["image_id"], inplace=True)
    # for i in range(len(raw_img_paths)):
    #     for raw_img_path in raw_img_paths:
    #         metadata_df["path_preprocessed"] = str(raw_img_path)
    #         break

    for x in dxs.items():
        average_amount_of_pics_per_class = round(
            len(metadata_df) / len(metadata_df[y_col].unique())
        )  # In Debug Mode it is 100 / 7 = 14, in Experiment Mode it is 10000 / 7 = 1428

        # Check if samples of class are below average
        if x[1] <= average_amount_of_pics_per_class:
            tmp_df = metadata_df.loc[metadata_df.dx == x[0]]
            # More than 1 * 14 = 14 samples will probably never be upsampled, but if you want to set that range up to whatever number you wish
            for i in range(15):
                average_amount_of_pics_per_class -= x[1]
                # Check if doubled-up samples are below average, so no more samples are created than it is wished to be
                if x[1] <= average_amount_of_pics_per_class:
                    iteration = x[1]
                # When there are just as many samples as the average amount we can break out of this for-loop
                elif average_amount_of_pics_per_class <= 0:
                    break
                elif x[1] > average_amount_of_pics_per_class:
                    iteration = x[1] - average_amount_of_pics_per_class

                for j in range(iteration):

                    image_id = tmp_df.iloc[j, :]["image_id"]
                    # FIXME weird pointer-issue, when I change a cell in single_row it also changes my tmp_df
                    single_row = tmp_df.iloc[j : (j + 1), :]

                    # Check where file exists
                    # if((os.path.isfile(str(DATA_RAW) + "/HAM10000_images_part_1/" + image_id + ".jpg"))):
                    #     image_path = str(DATA_RAW) + "/HAM10000_images_part_1/" + image_id + ".jpg"
                    # elif((os.path.isfile(str(DATA_RAW) + "/HAM10000_images_part_2/" + image_id + ".jpg"))):
                    #     image_path = str(DATA_RAW) + "/HAM10000_images_part_2/" + image_id + ".jpg"
                    # else:
                    #     raise (ValueError("Image does not exist in HAM10000 directory."))

                    # Setup for Data Generator to augment images
                    # FIXME pictures always augment the same way, random_state is missing!!!
                    generator_params_train = {
                        "x_col": "path_preprocessed",
                        "y_col": "dx",
                        "batch_size": iteration,
                        "classes": clazzes,
                        "shuffle": False,
                    }

                    augmentation_params = {
                        "horizontal_flip": random.choice([True, False]),
                        "vertical_flip": random.choice([True, False]),
                        "shear_range": random.choice([0, 0.1, 0.2, 0.3, 0.4, 0.5]),
                        "rotation_range": random.choice([0, 90]),
                        "width_shift_range": random.choice([0, 0.1, 0.2, 0.3, 0.4, 0.5]),
                        "height_shift_range": random.choice([0, 0.1, 0.2, 0.3, 0.4, 0.5]),
                        "zoom_range": random.choice([0, 0.1, 0.2, 0.3, 0.4, 0.5]),
                        "fill_mode": random.choice(["constant", "nearest", "reflect", "wrap"]),
                    }

                    train_generator_instance = AlvucDataGenerator(**augmentation_params)

                    (
                        train_generator,
                        STEP_SIZE_TRAIN,
                        _,
                    ) = train_generator_instance.flow_from_metadata_df(
                        single_row,
                        target_size_x=IMG_SIZE_X,
                        target_size_y=IMG_SIZE_Y,
                        **generator_params_train,
                    )

                    batch, _ = train_generator.next()

                    augmented_image = Image.fromarray((batch[0] * 255).astype(np.uint8))
                    path = str(DATA_PREPROCESSED / (image_id + str(i) + ".jpg"))
                    augmented_image.save(path)

                    # augmented_image.save(str(DATA_PREPROCESSED / (image_id + j + "augmented.jpg")), "JPEG")
                    # TODO make sure image_id is always at index position 0,1 -> iat[0,1], if not run loop to check that out
                    single_row.iat[0, 1] = str(single_row.iloc[0]["image_id"] + str(i))

                    metadata_df = metadata_df.append(single_row, ignore_index=True)

                    # pd.concat([metadata_df][single_row], ignore_index=True)

                    # tmp_df['path_preprocessed']

                    # tmp_df.iloc[1,:]['path_preprocessed']

                    # metadata_df.copy(
                    #     metadata_df.loc[metadata_df.dx.isin([str(x[0])])].index[0], inplace=True
                    # )

    metadata_df.to_csv(DATA_RAW / "HAM10000_metadata_upsampled.csv")
    labeled_augmented = metadata_df.loc[:, ["image_id", "dx"]]
    labeled_augmented.to_csv(DATA_LABEL_STATE / "labeled_upsampled.csv")


# %%
if __name__ == "__main__":
    # Define & Initialize paths
    dvc_paths = config.DvcPaths()

    DATA_RAW = dvc_paths.raw_data_path
    DATA_PREPROCESSED = dvc_paths.preprocessed_path
    DATA_PREPROCESSED.mkdir(parents=True, exist_ok=True)

    with open(dvc_paths.params_path) as file:
        params = yaml.safe_load(file)

    complete_metadata = pd.read_csv(DATA_RAW / "HAM10000_metadata.csv")

    # resize images to the follow values
    IMG_SIZE_X = params["PREPROCESS"]["IMG_SIZE_X"]
    IMG_SIZE_Y = params["PREPROCESS"]["IMG_SIZE_Y"]
    RESIZE_METHOD = params["PREPROCESS"]["RESIZE_METHOD"]
    UPSAMPLE = params["PREPROCESS"]["UPSAMPLE"]

    # Create named tuple for image_size
    OUTPUT_IMAGE_SIZE = ImageDimensions(width=IMG_SIZE_X, height=IMG_SIZE_Y)

    # Collect all jpgs in all subdirectories
    raw_img_paths = list(DATA_RAW.glob(r"**/*.jpg"))
    num_samples = len(raw_img_paths)
    logger.info(f"Found {num_samples} images, starting resizing.")

    # Call preprocessing function
    for raw_img_path in raw_img_paths:
        output_image = preprocess_image(raw_img_path, RESIZE_METHOD, OUTPUT_IMAGE_SIZE)
        output_image.save(str(DATA_PREPROCESSED / (raw_img_path.stem + ".jpg")), "JPEG")

    logger.info("Resizing images finished.")

    if UPSAMPLE:
        upsample_dataframe(
            complete_metadata, num_samples, y_col="dx", width=IMG_SIZE_X, height=IMG_SIZE_Y
        )
