import numpy as np
import skimage.io as io
from skimage.transform import resize


def preprocess_image(path_img_raw, path_imgs_resized, size, skin_df, rescale=True):
    """
    go through all image_ids in total_data_dict and check if they are allready resized
    if not resize them to given size and save them to img_path_resized
    """

    # Get the dx based on image id
    dx = skin_df[skin_df["image_id"] == path_img_raw.stem]["dx"].values[0]
    img = io.imread(path_img_raw)
    img = resize(img, size)

    # Rescale image to values between [0,1]
    if rescale:
        img = img / 255.0

    # Save the image under the new path whilst keeping the old filename
    np.save(str(path_imgs_resized / dx / (path_img_raw.stem + ".npy")), img)
