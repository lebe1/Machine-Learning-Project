import os
from pathlib import Path

import yaml
from loguru import logger

"""
This skript includes methods to read configurations from `dvc_paths.yaml` file.
To read the config file it is necessary to find the file. Therefore `get_project_config_path`
searches the environment variables for Path definition and searches the the current working
directory and subfolders for the config file.
"""


def get_project_config_path() -> Path:
    """
    Returns absolute path to `dvc_paths.yaml` file in the root directory of this lifedata-project.
    If file was not found in current working directory or its parent folders give out print error with steps to rectify this issue.
    """

    # NOTE: Search for `dvc_paths.yaml` in actual working directory and parent directorys (subdirectorys excuded)
    dir_count = 0
    cwd = Path.cwd()
    filepath = [p.absolute() for p in cwd.glob("dvc_paths.yaml")]
    while not filepath:
        try:
            filepath = [p.absolute() for p in cwd.parents[dir_count].glob("dvc_paths.yaml")]
        except FileNotFoundError:
            break
        dir_count += 1

    if not filepath:
        logger.error(
            "Datei wurde im aktuellen Verzeichnis und den darüberliegenden Verzeichnissen nicht gefunden"
        )
    os.environ["LIFEDATA_CONFIG_PATH"] = str(filepath[0])
    return filepath[0]


# Load params
def read_configs(project_config_file):
    """
    Method to read the `dvc_paths.yaml` - file
    """
    with open(project_config_file) as file:
        configs = yaml.safe_load(file)
    return configs


class DvcPaths:
    """
    Calss to read all dvc paths

    # NOTE: To define further paths add them in "dvc_paths.yaml" and append them in
    this init function
    """

    def __init__(self):
        self,
        # The root directory is the parent directory of the `lifedata_config.yml`
        project_root_path = get_project_config_path().parent
        self.configs = read_configs(get_project_config_path())
        self.raw_data_path = project_root_path / self.configs["raw_data_path"]
        self.analysis_path = project_root_path / self.configs["analysis_path"]
        self.preprocessed_path = project_root_path / self.configs["preprocessed_path"]
        self.label_state_path = project_root_path / self.configs["label_state_path"]
        self.train_test_split_path = project_root_path / self.configs["train_test_split_path"]
        self.model_path = project_root_path / self.configs["model_path"]
        self.eval_path = project_root_path / self.configs["eval_path"]
        self.explain_path = project_root_path / self.configs["explain_path"]
        self.query_path = project_root_path / self.configs["query_path"]
        self.params_path = project_root_path / self.configs["params_path"]


class Configs:
    """
    Class to read all project configurations

    # NOTE: To define further configurations add them in "dvc_paths.yaml" and append them in
    this init function
    """

    def __init__(self):
        self,
        get_project_config_path()
        self.configs = read_configs(Path(os.environ["LIFEDATA_CONFIG_PATH"]))
        self.project_name = self.configs["project_name"]
        self.git_remote_url = self.configs["git_remote_url"]
        self.data_type = self.configs["data_type"]
        self.labels = self.configs["labels"]
        self.label_type = self.configs["label_type"]
        self.query_strategy = self.configs["query_strategy"]
        self.interpretation_method = self.configs["interpretation_method"]
