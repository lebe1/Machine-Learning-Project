from pathlib import Path

import pandas as pd


def create_image_paths_in_df(df: pd.DataFrame, img_path: Path) -> pd.DataFrame:
    """
    This helper function takes a DataFrame with ImageIDs and an absolute path <img_path> where the
    image files are located. It will create a new column called "img_path" which consists of a
    concatenation of the <img_path> argument and the ImageID column. At the end converted as string.

    Args:
        df (pd.DataFrame): DataFrame which contains the ImageIDs

    Returns:
        pd.DataFrame: DataFrame with the added column "img_path" (concatenation of <img_path> argument
         and the ImageID column)
    """

    # Only if the df is not empty
    if len(df) > 0:
        df["img_path"] = df.apply(lambda row: str(img_path / (row["image_id"] + ".jpg")), axis=1)

    return df
