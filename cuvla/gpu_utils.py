import os
import subprocess as sp

from loguru import logger

# Set Tensorflow Log Level to "Warning"
# Source: https://stackoverflow.com/questions/38073432/how-to-suppress-verbose-tensorflow-logging
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"  # or any {'0', '1', '2', '3'}
import tensorflow as tf  # noqa [E402] We can only import tensorflow after setting environ var


def mask_unused_gpus(count_gpus_unmasked, minimum_available_ram=1024):
    """
    Automatically hide GPUs in use from Tensorflow / Keras via setting
    the env variable `CUDA_VISIBLE_DEVICES` in the operating system

    Parameters:
    -----------
    count_gpus_unmasked : int
      Count of GPUs to leave unmasked
    minimum_available_ram : int
      Minimum RAM in MB
    """
    try:
        CUDA_VISIBLE_DEVICES = os.environ["CUDA_VISIBLE_DEVICES"]
        logger.info(f"Using already set devices {CUDA_VISIBLE_DEVICES}")
        return
    except KeyError:
        pass

    ACCEPTABLE_AVAILABLE_MEMORY = minimum_available_ram
    COMMAND = "nvidia-smi --query-gpu=memory.free --format=csv"

    try:

        def output_to_list(out):
            return out.decode("ascii").split("\n")[:-1]

        memory_free_info = output_to_list(sp.check_output(COMMAND.split()))[1:]
        memory_free_values = [int(x.split()[0]) for i, x in enumerate(memory_free_info)]
        available_gpus = [
            i for i, x in enumerate(memory_free_values) if x > ACCEPTABLE_AVAILABLE_MEMORY
        ]

        if len(available_gpus) < count_gpus_unmasked:
            raise ValueError("Found only %d usable GPUs in the system" % len(available_gpus))
        os.environ["CUDA_VISIBLE_DEVICES"] = ",".join(
            map(str, available_gpus[:count_gpus_unmasked])
        )
        logger.info(
            f'Select GPU # {os.environ["CUDA_VISIBLE_DEVICES"]} with {memory_free_info[available_gpus[0]]} free memory.'
        )

    except Exception as e:
        logger.info('"nvidia-smi" is probably not installed. GPUs are not masked', e)

    ################################
    # Only grab necessary GPU RAM
    ################################
    gpus = tf.config.experimental.list_physical_devices("GPU")
    if gpus:
        try:
            # Currently, memory growth needs to be the same across GPUs
            for gpu in gpus:
                tf.config.experimental.set_memory_growth(gpu, True)
            logical_gpus = tf.config.experimental.list_logical_devices("GPU")
            logger.info(
                f"Set `Memory Growth` for {len(gpus)} Physical GPUs, {len(logical_gpus)} Logical GPUs"
            )
        except RuntimeError as e:
            # Memory growth must be set before GPUs have been initialized
            logger.error(e)
