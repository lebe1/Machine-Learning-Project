import pandas as pd


def qs_random(unlabeled_df: pd.DataFrame, query_set_size: int) -> pd.DataFrame:
    """Randomly select `query_set_size` amount of records from unlabeled_df

    Args:
        unlabeled_df (pd.DataFrame): Each row contains one unlabeled record
        query_set_size (int): Amount of records to be sampeled

    Returns:
        (pd.DataFrame): Subset of unlabeled_df selected to be annotated
    """

    return unlabeled_df.sample(query_set_size)
