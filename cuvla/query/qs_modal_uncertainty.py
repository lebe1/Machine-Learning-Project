import numpy as np
import pandas as pd
import yaml
from modAL.models import ActiveLearner
from modAL.uncertainty import uncertainty_sampling
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier

from alvuc.functions import config
from alvuc.util import AlvucDataGenerator

# Define & initialize paths
dvc_paths = config.DvcPaths()

with open(dvc_paths.params_path) as file:
    params = yaml.safe_load(file)
IMG_SIZE_X = params["PREPROCESS"]["IMG_SIZE_X"]
IMG_SIZE_Y = params["PREPROCESS"]["IMG_SIZE_Y"]


def qs_modal_uncertainty(
    unlabeled_df: pd.DataFrame,
    query_set_size: int,
    classifier: KerasClassifier,
) -> pd.DataFrame:
    """Use the modal uncertainty query strategy on the unlabeled data

    Args:
        unlabeled_df (pd.DataFrame): each row contains information about one unannotated sample, needs a column named `path_preprocessed`
        query_set_size (int): How many samples to query
        classifier ([type]): A tensorflow.keras.wrappers.scikit_learn.KerasClassifier, which must have the method `predict_proba`

    Returns:
        pd.DataFrame: Subset of unlabeled_df selected to be annotated
    """

    learner = ActiveLearner(estimator=classifier, query_strategy=uncertainty_sampling)

    datagenerator_params = {
        "x_col": "path_preprocessed",
        "batch_size": 1,
        "class_mode": None,
    }

    datagenerator_instance = AlvucDataGenerator()
    unlabeled_generator, _, _ = datagenerator_instance.flow_from_metadata_df(
        unlabeled_df, target_size_x=IMG_SIZE_X, target_size_y=IMG_SIZE_Y, **datagenerator_params
    )

    data_list = []
    batch_index = 0

    # TODO this is very inefficient, since it loads the complete unlabeled dataset into RAM one by one
    while batch_index <= unlabeled_generator.batch_index:
        data = unlabeled_generator.next()
        data_list.append(data[0])
        batch_index = batch_index + 1

    # now, data_array is the numeric data of all images. This array can be quite large
    data_array = np.asarray(data_list)

    query_idx, query_data = learner.query(X_pool=data_array, n_instances=query_set_size)

    queryset = unlabeled_df.iloc[query_idx]["image_id"]

    return queryset
