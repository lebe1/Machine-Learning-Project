from enum import Enum

import numpy as np
import pandas as pd
import yaml
from loguru import logger

import alvuc.gpu_utils
from alvuc.functions import config

alvuc.gpu_utils.mask_unused_gpus(count_gpus_unmasked=1, minimum_available_ram=9000)


class QUERY_STRATEGIES(Enum):
    RANDOM = "random"
    UNCERTAINTY = "uncertainty"


def load_keras_model():
    from tensorflow.keras.models import load_model

    return load_model(dvc_paths.model_path / "model.h5", compile=True)


def run_query_strategy(
    unlabeled_df: pd.DataFrame,
    query_strategy: QUERY_STRATEGIES,
    random_state: int,
    query_set_size: int,
) -> pd.DataFrame:
    """
    TODO Doc
    """

    # reduce queryset size for last iteration
    if len(unlabeled_df) < query_set_size:
        query_set_size = len(unlabeled_df)

    np.random.seed(random_state)

    logger.info(f"Querying with strategy {query_strategy}")
    if query_strategy == QUERY_STRATEGIES.RANDOM.value:
        import qs_random

        queryset_df = qs_random.qs_random(unlabeled_df, query_set_size)
    elif query_strategy == QUERY_STRATEGIES.UNCERTAINTY.value:

        import qs_modal_uncertainty
        from tensorflow.keras.wrappers.scikit_learn import KerasClassifier

        scikit_wrapped_classifier = KerasClassifier(load_keras_model)
        # Rabefabis guess: SciKit API expects a `predict_proba` to exist, but Keras Models already return probabilites per default when called with `predict`
        # The modAL Tutorial works, because they call KerasClassifier.fit at some point, which initializes the model the correct way
        # Link to tutorial: https://modal-python.readthedocs.io/en/latest/content/examples/Keras_integration.html
        # Since we don't need `.fit`, we need to create a method called `predict_proba`, which does exactly what `predict` does
        model = load_keras_model()
        scikit_wrapped_classifier.model = model
        scikit_wrapped_classifier.model.predict_proba = scikit_wrapped_classifier.model.predict

        queryset_df = qs_modal_uncertainty.qs_modal_uncertainty(
            unlabeled_df, query_set_size, scikit_wrapped_classifier
        )

    else:
        raise NotImplementedError()

    return queryset_df


def filter_test_data_from_unlabeled_df(
    unlabeled_df: pd.DataFrame, test_df: pd.DataFrame, train_test_strategy: str
) -> pd.DataFrame:
    """This function takes an unlabeled_df and filters out any records found in test_df

    Args:
        unlabeled_df ([pd.DataFrame]): DataFrame with list of unlabeled records
        test_df ([pd.DataFrame]): DataFrame containing list of test records
        train_test_strategy (str): Which strategy was used during the ML pipeline

    Returns:
        [pd.DataFrame]: filtered DataFrame without test_df records
    """

    logger.info(f"Filtering {len(test_df)} test records from unlabeled_df")
    filtered_unlabeled_df = unlabeled_df[~unlabeled_df["image_id"].isin(test_df["image_id"])]

    return filtered_unlabeled_df


if __name__ == "__main__":

    dvc_paths = config.DvcPaths()
    # Create output dir of this script
    dvc_paths.query_path.mkdir(exist_ok=True, parents=True)

    test_df = pd.read_csv(dvc_paths.train_test_split_path / "test.csv")

    with open(dvc_paths.params_path) as file:
        params = yaml.safe_load(file)

    TRAIN_TEST_STRATEGY = params["TRAIN_TEST"]["TRAIN_TEST_STRATEGY"]
    QUERY_STRATEGY = params["QUERY"]["QUERY_STRATEGY"]
    RANDOM_STATE = params["RANDOM_STATE"]
    QUERY_SET_SIZE = params["QUERY"]["QUERY_SET_SIZE"]

    unlabeled_df = pd.read_csv(dvc_paths.label_state_path / "unlabeled.csv")

    unlabeled_df = filter_test_data_from_unlabeled_df(unlabeled_df, test_df, TRAIN_TEST_STRATEGY)

    unlabeled_df["path_preprocessed"] = unlabeled_df.apply(
        lambda row: str(dvc_paths.preprocessed_path / (row["image_id"] + ".jpg")), axis=1
    )

    if len(unlabeled_df) == 0:
        logger.warning("After filtering the unlabeled dataframe is empty, creating empty query set")
        queryset_df = pd.DataFrame(columns=["image_id"])
    else:
        queryset_df = run_query_strategy(unlabeled_df, QUERY_STRATEGY, RANDOM_STATE, QUERY_SET_SIZE)

    logger.info(f"Saving {len(queryset_df)} samples to queryset.csv")
    queryset_df.to_csv(dvc_paths.query_path / "queryset.csv", index=False)
