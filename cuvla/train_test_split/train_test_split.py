# %%
import random
from typing import Tuple

import pandas as pd
import yaml
from loguru import logger
from sklearn.model_selection import GroupShuffleSplit

from alvuc.functions import config

dvc_paths = config.DvcPaths()

with open(dvc_paths.params_path) as file:
    params = yaml.safe_load(file)
DOWNSAMPLE = params["TRAIN_TEST"]["DOWNSAMPLE"]
IMG_SIZE_X = params["PREPROCESS"]["IMG_SIZE_X"]
IMG_SIZE_Y = params["PREPROCESS"]["IMG_SIZE_Y"]
BATCH_SIZE = params["MODEL_TRAINING"]["BATCH_SIZE"]


def train_test_split_static(
    metadata_df: pd.DataFrame, random_state: int, y_col: str, group_col: str, test_size: int
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Train_test_split_static picks {test_size} samples per class into the test set
    Remaining records will be put in the train set.

    ATTENTION: A test_size setting of 10 does not necessarily guarantee that your test set is evenly spread
    with exactly 10 samples per class because it is possible that a group_col might contribute more than one sample.

    Args:
        metadata_df (dataframe): Dataframe to be split into train and test sets
        random_state (int): The random_state defines the pandas sample random state
        y_col (str): Denotes the column name of your target class to be predicted
        group_col (str): Denotes the column name containing the groups which will either be split into test or train set, but not both
                         Example: Three samples share the same group_col value. It cannot happen that, for example,
                         2 of them go into train and 1 into test. All 3 will either be part of train or test.
        test_size (int): The sample count of each class for the test set (see "ATTENTION" remark above why count could be more)

    Returns:
        Tuple[pd.DataFrame, pd.DataFrame]: train DataFrame, test DataFrame

    """
    # metadata_df = upsample_dataframe(metadata_df, y_col="dx")
    # Get frequency counts for your target variables and save dictionary
    dxs = metadata_df[y_col].value_counts()
    dxs = dxs.to_dict()

    # Get the minimum value of all key/value pairs and check if the test_size is smaller
    # than the minimum count of every diagnosis
    min_dx_value_counts = min(dxs.items(), key=lambda x: x[1])

    # TODO: My original thought was that the test_size should be smaller than the count of the minority class
    # It seems that our debug metadata.csv only includes 1 sample for the minority class. So if this one sample is chosen
    # for the test split, then the respective class would not be available anymore for training.
    # min_dx_value_counts[0]: target class, min_dx_value_counts[1]: minimum count of target class
    if test_size > min_dx_value_counts[1]:
        raise Exception(
            f"Your test_size ({test_size}) per class must be smaller (equal) than the frequency count of the minority class {min_dx_value_counts[0]} ({min_dx_value_counts[1]} samples)."
        )

    logger.info(
        f"Creating test split with {test_size} samples per class from a total of {len(metadata_df)}; everything else into train split"
    )

    collect_lesion_id = []
    for y, _ in dxs.items():
        # Filter metadata_df for target class
        dx_subset_df = metadata_df[metadata_df[y_col] == y]
        logger.debug(f"Sampling for class {y} from a total subset count of {len(dx_subset_df)}.")
        # Group by lesion_id and get aggregated counts how many times each lesion_id is present in the subset
        group_by = dx_subset_df.groupby([group_col])
        lesion_id_counts = group_by.size().to_frame(name="count")
        cur_count = 0
        # Randomly shuffle the whole dataframe (frac=1) containing lesion_ids and their counts;
        # iterate over each row until a count of test_size is reached
        for row in lesion_id_counts.sample(frac=1, random_state=random_state).iterrows():
            row_index = row[0]
            row_content = row[1]
            cur_count += row_content["count"]
            collect_lesion_id.append(row_index)
            if cur_count >= test_size:
                logger.info(f"Added {cur_count} sample(s) for target class '{y}' to test split.")
                break

    # All collected lesion_ids are part of test, everything else into train
    test = metadata_df[metadata_df[group_col].isin(collect_lesion_id)]
    train = metadata_df[~metadata_df[group_col].isin(test[group_col])]

    return train, test


def train_test_split_proportional(
    metadata_df: pd.DataFrame,
    random_state: int,
    test_size: float,
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """

    Train_test_split_proporational splits the metadata file into a train and test set with
    class distributions similar to those in the overall population (metadata)

    Args:
        metadata_df (pd.DataFrame): Metadata Dataframe to be split into train and test sets
        random_state (int): The random_state defines the GroupShuffleSplit random state
        test_size (float): The proportion of groups to include in the test split

    Returns:
        Tuple[pd.DataFrame, pd.DataFrame]: The train DataFrame and the test DataFrame

    """

    if len(metadata_df) == 0:
        logger.warning("No data to train-test-split. Returning empty dataframes.")
        return metadata_df.copy(), metadata_df.copy()

    # GroupShuffleSplit to ensure that multiple images of the same lesion are either in train or in test
    # See https://git.rz.uni-augsburg.de/swtpvs-lifedata/alvuc/-/issues/114#note_37002
    train_idx, test_idx = next(
        GroupShuffleSplit(test_size=test_size, random_state=random_state).split(
            metadata_df, groups=metadata_df["lesion_id"]
        )
    )

    # generate the train und test DataFrame
    train = metadata_df.iloc[train_idx]
    test = metadata_df.iloc[test_idx]

    return train, test


def downsample_dataframe(metadata_df: pd.DataFrame, y_col: str) -> pd.DataFrame:

    """
    downsample_dataframe decreases every class with more than the average amount of samples per class

    Args:
        metadata_df (dataframe): Dataframe to be reduced
        y_col (str): Denotes the column name of your target class to be predicted

    Returns:
        pd.DataFrame

    """

    if len(metadata_df) == 0:
        logger.warning("No data to downsample. Returning empty dataframes.")
        return metadata_df

    # Get frequency counts for your target variables and save dictionary
    dxs = metadata_df[y_col].value_counts()
    dxs = dxs.to_dict()

    # In Debug Mode it is 100 / 7 = 14, in Experiment Mode it is 10000 / 7 = 1428
    average_samples_per_class = round(len(metadata_df) / len(metadata_df[y_col].unique()))
    logger.info(
        f"Downsample Dataset. Dataset will be shrunk down to {average_samples_per_class} samples per class."
    )

    # Iterate over every single class and delete first row of classes that have more pics than the average amount of pics per class
    for dx, count in dxs.items():
        if count > average_samples_per_class:
            for iteration in range(count - average_samples_per_class):
                metadata_df.drop(
                    metadata_df.loc[metadata_df[y_col].isin([str(dx)])].index[
                        random.randint(0, (count - (iteration + 1)))
                    ],
                    inplace=True,
                )

    return metadata_df


# %%
def main():

    dvc_paths = config.DvcPaths()
    DATA_RAW = dvc_paths.raw_data_path
    DATA_LABEL_STATE = dvc_paths.label_state_path
    DATA_TRAINTEST = dvc_paths.train_test_split_path
    DATA_TRAINTEST.mkdir(parents=True, exist_ok=True)

    with open(dvc_paths.params_path) as file:
        params = yaml.safe_load(file)
    TEST_SIZE = params["TRAIN_TEST"]["TEST_SIZE"]
    TRAIN_TEST_STRATEGY = params["TRAIN_TEST"]["TRAIN_TEST_STRATEGY"]
    RANDOM_STATE = params["RANDOM_STATE"]
    DOWNSAMPLE = params["TRAIN_TEST"]["DOWNSAMPLE"]

    # if UPSAMPLE:
    #     labeled_csv = pd.read_csv(DATA_LABEL_STATE / "labeled_upsampled.csv")
    #     complete_metadata = pd.read_csv(DATA_RAW / "HAM10000_metadata_upsampled.csv")
    # else:
    labeled_csv = pd.read_csv(DATA_LABEL_STATE / "labeled.csv")
    complete_metadata = pd.read_csv(DATA_RAW / "HAM10000_metadata.csv")

    logger.info(f"Creating Datasets with TRAIN_TEST_STRATEGY = {TRAIN_TEST_STRATEGY}")

    if TRAIN_TEST_STRATEGY == "proportional":
        # Take a proportion of *all* data, assume test data will be labeled
        assert (
            type(TEST_SIZE) == float
        ), "TEST_SIZE needs to be a percentage of dataset (float) for proportional train test split"
        train, test = train_test_split_proportional(complete_metadata, RANDOM_STATE, TEST_SIZE)
    elif TRAIN_TEST_STRATEGY == "proportional_labeled":
        # Take a proportion of only the labeled data, test set will grow with labeled data
        assert (
            type(TEST_SIZE) == float
        ), "TEST_SIZE needs to be a percentage of dataset (float) for proportional train test split"

        available_data = complete_metadata[
            complete_metadata["image_id"].isin(labeled_csv["image_id"])
        ]
        train, test = train_test_split_proportional(
            metadata_df=available_data, random_state=RANDOM_STATE, test_size=TEST_SIZE
        )
    elif TRAIN_TEST_STRATEGY == "static":
        # Create test set with TEST_SIZE (int) amount of samples for each class; everything else into train set
        assert (
            type(TEST_SIZE) == int
        ), "TEST_SIZE needs to be a count of samples (int) per class for static train test split"

        train, test = train_test_split_static(
            metadata_df=complete_metadata,
            random_state=RANDOM_STATE,
            y_col="dx",
            group_col="lesion_id",
            test_size=TEST_SIZE,
        )
    else:
        raise ValueError(f"Unknown TRAIN_TEST_STRATEGY:{TRAIN_TEST_STRATEGY}")

    if DOWNSAMPLE:
        train = downsample_dataframe(train, y_col="dx")

    # Keep only labeled data in training set
    train = train[train["image_id"].isin(labeled_csv["image_id"])]

    logger.info(f"Storing train split with length {len(train)}")
    train.to_csv(DATA_TRAINTEST / "train.csv")
    logger.info(f"Storing test split with length {len(test)}")
    test.to_csv(DATA_TRAINTEST / "test.csv")


if __name__ == "__main__":
    main()

# %%
