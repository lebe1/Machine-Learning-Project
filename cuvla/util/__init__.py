# %%
from contextlib import redirect_stdout
from typing import Any
from typing import List
from typing import Tuple

import numpy as np
import pandas as pd
import yaml
from keras_preprocessing.image import DataFrameIterator
from loguru import logger
from tensorflow.keras.preprocessing.image import ImageDataGenerator

from alvuc.functions import config

# TODO
# FIXME Der tensorflow.keras.preprocessing.imge Import ist abhängig von der Transfer Learning Modell Architektur (z.B. Resnet50, EfficientNetB0...)

# Define & Initialize paths, read values for image size from params.yaml
dvc_paths = config.DvcPaths()
with open(dvc_paths.params_path) as file:
    params = yaml.safe_load(file)
RANDOM_STATE = params["RANDOM_STATE"]
MODEL_ARCH = params["MODEL_TRAINING"]["MODEL_ARCH"]
PREPROCESS_INPUTS = params["MODEL_TRAINING"]["PREPROCESS_INPUTS"]

if MODEL_ARCH == "EfficientNetB0":
    from tensorflow.keras.applications.efficientnet import preprocess_input
elif MODEL_ARCH == "MobileNetV2":
    from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
elif MODEL_ARCH == "ResNet50":
    from tensorflow.keras.applications.resnet import preprocess_input


class AlvucDataGenerator:
    def __init__(self, use_augmentation=False, **imageDataGenerator_kwargs: Any):
        """Initialize Generator

        Args:
            use_augmentation (bool, optional): Use Data Augmentation to modify images by little rotating, shifting etc.
                                                Data Augmentation prevents overfitting. Defaults to False.
        """

        self.imageDataGenerator_kwargs = imageDataGenerator_kwargs
        # set rescale to default
        if "rescale" not in self.imageDataGenerator_kwargs:
            self.imageDataGenerator_kwargs["rescale"] = 1.0 / 255.0

        # FIXME do not use global variable here
        if PREPROCESS_INPUTS:
            logger.info("Using prepreocessing function for pretrained models.")
            self.imageDataGenerator_kwargs["preprocessing_function"] = preprocess_input

    def initialize_generator(
        self,
        dataframe: pd.DataFrame,
        target_size_x: int,
        target_size_y: int,
        **flow_from_dataframe_kwargs: Any,
    ) -> None:
        """Method to (re-)initialize ImageDataGenerator because .reset() for the Generator object
            does not work as intended

        Args: See below method flow_from_metadata_df()

        Returns:
            `flow_from_dataframe` iterator
        """
        gen = ImageDataGenerator(**self.imageDataGenerator_kwargs)
        self.flow_from_dataframe = gen.flow_from_dataframe(
            dataframe, target_size=(target_size_y, target_size_x), **flow_from_dataframe_kwargs
        )

    def flow_from_metadata_df(
        self,
        dataframe: pd.DataFrame,
        target_size_x: int,
        target_size_y: int,
        **flow_from_dataframe_kwargs: Any,
    ) -> Tuple[DataFrameIterator, int, List[int]]:
        """Custom method for not only returning a `flow_from_dataframe` iterator, but also
        returning `batches_per_epoch` (step_size) and list of y_true labels

        Args:
            dataframe (DataFrame):  Pandas dataframe containing the filepaths of the images (x_col)
                                    and classes (y_col) in a string column
            target_size_x (int): count of columns the image will be resized to
            target_siz_y (int): count for rows the image will be resized to
            flow_from_dataframe_kwargs (dict): dictionary containing arguments for the `flow_from_dataframe` method
            for all arguments see: https://keras.io/api/preprocessing/image/#flowfromdataframe-method

        Raises:
            Exception: if `y_col` is defined for the generator to return targets, but no seed
                        argument is passed (Generator must receive a seed in order to initialize twice identically

        Returns:
            tuple(iterator, int, list): `flow_from_dataframe` iterator, int of `batches_per_epoch` and
                                        correctly ordered list of true labels `y_true`
        """

        # if no seed is passed, then use the RANDOM_STATE from params.yaml
        # If y_true is returned, a seed must be passed to guarantee that the generator
        # returns the same order for the epochs
        if "seed" not in flow_from_dataframe_kwargs:
            flow_from_dataframe_kwargs["seed"] = RANDOM_STATE
        # Check if y_col was passed as parameter. Only then y_true can be returned
        if "y_col" in flow_from_dataframe_kwargs:
            # If y_true is returned, a seed must be passed to guarantee that the generator
            # returns the same order for the epochs
            if flow_from_dataframe_kwargs["seed"] is not None:
                # Suppress standard logging message from Generator to avoid that the same is printed twice
                with redirect_stdout(None):
                    self.initialize_generator(
                        dataframe,
                        target_size_x=target_size_x,
                        target_size_y=target_size_y,
                        **flow_from_dataframe_kwargs,
                    )
                # Get the first batch, only then the order of the samples in index_array is populated
                next(self.flow_from_dataframe)
                self.y_true = [
                    self.flow_from_dataframe.classes[i]
                    for i in self.flow_from_dataframe.index_array
                ]
            else:
                raise ValueError(
                    "When passing `y_col, a `seed` must be passed as parameter to `flow_from_metadata_df` to guarantee the same order of samples. Otherwise `y_true` is in the wrong order"
                )
        else:
            self.y_true = []

        self.initialize_generator(
            dataframe,
            target_size_x=target_size_x,
            target_size_y=target_size_y,
            **flow_from_dataframe_kwargs,
        )
        self.batches_per_epoch = int(
            np.ceil(self.flow_from_dataframe.n / self.flow_from_dataframe.batch_size)
        )

        return (self.flow_from_dataframe, self.batches_per_epoch, self.y_true)


def _flatten_dict_generator(pyobj, keystring=""):
    """
    Flattens a dict by appending keys together with a `_`.

    Returns a generator

    Source: https://www.geeksforgeeks.org/python-convert-nested-dictionary-into-flattened-dictionary/
    """
    if type(pyobj) == dict:
        keystring = keystring + "_" if keystring else keystring
        for k in pyobj:
            yield from _flatten_dict_generator(pyobj[k], keystring + str(k))
    else:
        yield keystring, pyobj


def flatten_dict(my_dict):
    """
    Flattens a dict by appending keys together with a `_`.

    Example:
    --------
    ini_dict = {'geeks': {'Geeks': {'for': 7}},
            'for': {'geeks': {'Geeks': 3}},
            'Geeks': {'for': {'for': 1, 'geeks': 4}}}

    Return:
    {‘Geeks_for_geeks’: 4, ‘for_geeks_Geeks’: 3, ‘Geeks_for_for’: 1, ‘geeks_Geeks_for’: 7}
    """

    return {key: value for key, value in _flatten_dict_generator(my_dict)}
