import os

import pandas as pd

from alvuc.util import flatten_dict
from alvuc.util.mlflow_setup import mlflow_setup


def mlflow_log_model_params(
    train_df: pd.DataFrame, test_df: pd.DataFrame, model_params: dict
) -> str:
    """
    Function to handle the params logging of MLFlow during the training stage.
    Returns the MLFlow RunID.

    Args:
        train_df (pd.DataFrame): DataFrame containing the filenames and classes for training
        test_df (pd.DataFrame): DataFrame containing the filenames and classes for testing
        model_params (dict): Dictionary with the settings from params.yaml

    Returns:
        str: mlflow_run_id which is used to identify the same run across different stages for
            accurate logging
    """

    if os.getenv("MLFLOW_ACTIVE", "False") == "True":

        mlflow_params = flatten_dict(model_params)
        mlflow_params["training_set_size"] = len(train_df)
        mlflow_params["test_set_size"] = len(test_df)

        mlflow_run_id = mlflow_setup(
            parent_run_id=os.getenv("MLFLOW_PARENT_RUN_ID", None),
            experiment_name=os.getenv("MLFLOW_EXPERIMENT_NAME", "Default"),
            run_name=os.getenv("MLFLOW_RUN_NAME"),
            mlflow_tags={
                "commit_source": os.getenv("MLFLOW_EXPERIMENT_SOURCE_COMMIT"),
                "al_iteration": os.getenv("AL_ITERATION"),
                "commit_msg": os.getenv("MLFLOW_MESSAGE"),
            },
            mlflow_params=mlflow_params,
        )
    else:
        # Create empty/useless mlflow_run_id file, due to DVC requirements. See https://git.rz.uni-augsburg.de/swtpvs-lifedata/alvuc/-/issues/139
        mlflow_run_id = "mlflow wasn't active"

    return mlflow_run_id
