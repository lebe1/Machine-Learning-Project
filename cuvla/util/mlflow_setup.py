import os

from loguru import logger


def mlflow_setup(
    run_id: str = None,
    parent_run_id: str = None,
    run_name: str = None,
    server_uri: str = os.getenv("MLFLOW_TRACKING_SERVER_URI", "https://mlflow.ds-lab.org"),
    experiment_name: str = "Default",
    mlflow_tags: dict = {},
    mlflow_params: dict = {},
    mlflow_metrics: dict = {},
    every_n_iter: int = 1,
) -> str:
    """Creates a new experiment run on the MLFlow server and configures tensorflow to log to it

    Args:
        run_id (str, optional): ID of a previous MLFLow run. If provided, data will be logged to this run, otherwise a new run will get created. See https://mlflow.org/docs/latest/tracking.html#managing-experiments-and-runs-with-the-tracking-service-api
        parent_run_id (str, optional): ID of a parent run. If provided, this run will be nested below the parent run
        run_name (str, optional): Name of a run. Used only when `run_id` unspecified
        server_uri (str, optional): Defaults to the environment variable `MLFLOW_TRACKING_SERVER_URI`. If not set, then https://mlflow.ds-lab.org".
        experiment_name (str, optional): All runs under the same experiment name are comparable in the UI. See https://mlflow.org/docs/latest/tracking.html#organizing-runs-in-experiments. Defaults to "Default".
        mlflow_tags (dict, optional): Defaults to {}.
        mlflow_params (dict, optional): Defaults to {}.
        mlflow_metrics (dict, optional): Defaults to {}.
        every_n_iter (int, optional): After how many epochs results should be logged. Defaults to 1.

    Returns:
        str: Unique ID of this run on the server
    """
    import mlflow
    import urllib3

    # Accept any https certificate without warnings
    urllib3.disable_warnings()

    mlflow.set_tracking_uri(server_uri)
    mlflow.set_experiment(experiment_name)

    if parent_run_id is not None:
        mlflow.start_run(run_id=parent_run_id)
        mlflow.start_run(run_id=run_id, run_name=run_name, nested=True)
    else:
        mlflow.start_run(run_id=run_id, run_name=run_name)

    mlflow.tensorflow.autolog(every_n_iter=every_n_iter)
    mlflow.log_params(mlflow_params)
    mlflow.set_tags(mlflow_tags)
    mlflow.log_metrics(mlflow_metrics)
    mlflow_run_id = mlflow.active_run().info.run_id

    experiment_id = mlflow.get_experiment_by_name(experiment_name).experiment_id
    mlflow_run_url = server_uri + "/#/experiments/" + str(experiment_id) + "/runs/" + mlflow_run_id
    logger.info(f"Started new MLFlow run: {mlflow_run_url}")

    return str(mlflow_run_id)
