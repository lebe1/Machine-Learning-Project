# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.5.1
#   kernelspec:
#     display_name: alvuc
#     language: python
#     name: alvuc
# ---
# %% [markdown]
# # 2 Data Understanding
#
# This report describes the dataset used for the ALVUC and follows the guidelines from stage 2 of the [CRISP DM](https://api.semanticscholar.org/CorpusID:59777418)
# %% [markdown]
# Initial code setup
# %%
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.express as px
import yaml
from numpy.random import RandomState

import alvuc
from alvuc.functions import config

# Define & Initialize paths & Parameters
dvc_paths = config.DvcPaths()

# NOTE: You can change paths by changing them in `dvc_paths.yaml` file if you do
# so you also need to change them in `dvc.yaml file`. TO add further paths see config.py/DvcPaths
DATA_RAW = alvuc.get_project_root() / dvc_paths.raw_data_path

with open(dvc_paths.params_path) as file:
    params = yaml.safe_load(file)
    RANDOM_STATE = params["RANDOM_STATE"]
    rs = RandomState(RANDOM_STATE)
    print(RANDOM_STATE)
    RANDOM_STATE = rs.randint(1, 1244, size=1)
    print(RANDOM_STATE.astype(int))
    RANDOM_STATE = np.random.random_integers(0, 100 + 1)
    print(RANDOM_STATE)

# %% [markdown]
# ## 2.1 Data Collection Report

# %% [markdown]
# ### The dataset

# %% [markdown]
# - The ALVUC will be built upon the *HAM10000* dataset, available at the Harvard Dataverse:
#   - https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/DBW86T
#   - ```
#     Tschandl, Philipp, 2018, "The HAM10000 dataset, a large collection of multi-source dermatoscopic images of common pigmented skin lesions", https://doi.org/10.7910/DVN/DBW86T, Harvard Dataverse, V1, UNF:6:IQTf5Cb+3EzwZ95U5r0hnQ== [fileUNF]
#     ```
#   - At the time of writing, dataset version 1.2 is the most recent
# - It consists of a metadata table and 2 zip files, whose contents are stored in this project under `data/raw`
#   - Inside this project the data is versioned via DVC
#   - An automatic download is not yet possible due to license checkers which require manual user acceptance
#     - The license is the * Creative Commons Attribution-NonCommercial 4.0 International Public License ("Public License")*
#     - See `data/ham10000.license` for a copy of the text shown during the download
#   - This could however be solved via the ISIC-Archive API available here: https://isic-archive.com/api/v1#!/image/image_downloadMultiple
#
#
#

# %% [markdown]
# - The dataset is described in (1)
#   ```
#   Tschandl, P., Rosendahl, C. & Kittler, H. The HAM10000 dataset, a large collection of multi-source dermatoscopic images of common pigmented skin lesions. Sci Data 5, 180161 (2018). https://doi.org/10.1038/sdata.2018.161
#   ```
#   - > The 10015 dermatoscopic images of the HAM10000 training set were collected over a period of 20 years from two different sites, the Department of Dermatology at the Medical University of Vienna, Austria, and the skin cancer practice of Cliff Rosendahl in Queensland, Australia. The Australian site stored images and meta-data in PowerPoint files and Excel databases. The Austrian site started to collect images before the era of digital cameras and stored images and metadata in different formats during different time periods.
#   - Some manual corrections have taken place regarding cropping, centering and histogram corrections
#   - The images are only a subset of the originally available data, filtered for actual dermatoscopies, ignoring overiew and close-up images
#   - Additionally, images with uncertain diagnoses or collisions of diagnoses were filtered, except for melanomas in association with a nevus
#   - Finally, there was a manual quality review of each of the images in the final dataset
# - It consists of 10015 dermatoscopic images, each accompanied by metadata

# %% [markdown]
# **Conclusion for ALVUC:**
#
# The dataset is well researched and reviewed multiple times, in addition to having served as the basis for the [ISIC Challenge 2018](https://challenge2018.isic-archive.com/). It is an ideal candidate to develop the first iteration of the LIFEDATA framework against.
# Data imports could be handled automatically in the future, but since the dataset is stable this is not a pressing issue

# %% [markdown]
# ## 2.2 Data description report
#

# %% [markdown]
# - The dataset consists of 10015 images, stored in 2 directories
# - Additionally, there is a metadata csv file containing information about each image

# %%
# Load the metadata
metadata = pd.read_csv(DATA_RAW / "HAM10000_metadata.csv")

# %%
# Find all jpgs and assert it's the expected amount
img_paths = list(DATA_RAW.glob("**/*.jpg"))

# FIXME This assert is only removed in order for the report not to fail on the debugging dataset
# assert len(img_paths) == 10015, f"Expected 10015 images, found {len(img_paths)}"

# %%
example_img_paths = [
    img_paths[i] for i in np.random.random_integers(low=0, high=len(img_paths), size=12)
]


def plots(ims, figsize=(15, 7.5), rows=3):  # 12,6
    f = plt.figure(figsize=figsize)
    f.suptitle("Example Images", fontsize=16)
    cols = len(ims) // rows if len(ims) % 2 == 0 else len(ims) // rows + 1
    for i in range(len(ims)):
        f.add_subplot(rows, cols, i + 1)
        plt.imshow(mpimg.imread(ims[i]))


plots(example_img_paths)

# %%
example_image = mpimg.imread(example_img_paths[0])

# %%
example_image.shape

# %%
# Some example metadata
metadata.head()

# %% [markdown]
# - Of the available metadata, the following columns are relevant for ALVUC
#   - **lesion_id:** The same lesion can appear on mutliple images
#   - **image_id:** Unique for each image
#   - **dx:** The diagnosis
#     - the predicted value of the machine learning model
#     - Described further in (1)
# - The following columns only serve as additional information
#   - **dx_type:** how the diagnosis was made. Described further in (1)
#   - **age** & **sex**: Self explantory
#   - **localization:** Where on the body the lesion is located
#

# %% [markdown]
# (1) describes additional datasets, which could be used to supplement the original HAM10000, see [this table](https://www.nature.com/articles/sdata2018161/tables/2)

# %% [markdown]
# **Conclusion for ALVUC:**
# - The images have a size of 600x450 pixels, and are in sufficient resolution for a human to detect the various classes
# - Therefore they are more than enough for a machine learning model to detect classes. Existing machine learning models actually reduce the resolution

# %% [markdown]
# ### Diagnoses
#
# - How many are there?
# - How are the classes distributed?
# - Comments based on (1)


# %%
px.bar(metadata["dx"].value_counts(), title="Count of Diagnoses")

# %% [markdown]
# **Observation:**
# - `nv` (*Melanocytic nevi*) are by far the most prevalent group in the dataset, which corresponds to the distribution in the real world and represents our "healthy" class for the Active Learning Use Case.
# - `mel` (*Melanoma*) is the second most common class and serves as minimum goal for our machine learning model to differentiate between it and nevi
# - `df` (*Dermatofibroma*) and `vasc` (*Vascular skin lesions*) only appear around 150 times, which might pose a challenge for the machine learning model
# - `bkl` (*Benign keratosis*) seems to be the 3rd largest class, however according to (1) it consists of three subgroups which look different dermatoscopically. This could pose a challenge for our machine learning model

# %% [markdown]
# For more medical background information on each class see (1)

# %% [markdown]
# ### Unique lesions & multiple images per lesion
#
# - How often does this occur?
# - Could this be a problem for ALVUC?

# %%
unique_lesions = len(metadata["lesion_id"].unique())
print(f"There are {unique_lesions} unique lesions in the dataset")

# %%
# How many images does each lesion_id have?
imgs_per_lesion = metadata[["lesion_id", "image_id"]].groupby("lesion_id").count()
imgs_per_lesion = imgs_per_lesion.rename(columns={"image_id": "count_corresponding_images"})
px.bar(
    imgs_per_lesion["count_corresponding_images"].value_counts(),
    title="How many images exist per lesion?",
)

# %%
# Data from the plot as table:
imgs_per_lesion["count_corresponding_images"].value_counts()

# %%
# Which classes are affected by multiple images?
dx_grps = metadata[["lesion_id", "image_id", "dx"]].groupby("dx").groups
fig, axs = plt.subplots(len(dx_grps), sharex=True, tight_layout=True)
fig.set_size_inches(7.5, 15)
for i, (dx, dx_grp_idxes) in enumerate(dx_grps.items()):
    imgs_per_dx_and_lesion = (
        metadata[["lesion_id", "image_id"]].iloc[dx_grp_idxes].groupby("lesion_id").count()
    )
    axs[i].hist(imgs_per_dx_and_lesion["image_id"])
    axs[i].set_title(dx)

fig.show()


# %% [markdown]
# **Conclusion:** For the class `mel` there are actually more lesions with 2 images than with a single image, otherwise single images for a lesion are the norm

# %% [markdown]
# **Conclusion for ALVUC:**
#
# With regards to ALVUC, all of the available images are relevant, some metadata information can be ignored (age, sex, localization). The rare classes will provide ample opportunity for the active learning algorithms to highlight their potential, and the issue with multiple images per lesion shouldn't affect the machine learning model too much, since the ISIC Challenge 2018 produced functional models. Since we're not interested in using the developed models for diagnostic purposes on patients, the requirements regarding to the final performance of the model are not as strict.

# %% [markdown]
# ## 2.3 Data Exploration Report

# %% [markdown]
# A deeper dive into the available data

# %%
# Check for missing values
metadata.isnull().sum()

# %% [markdown]
# **Conclusion for ALVUC:**
# The age values are not of high importance for our use case, and only a few are missing

# %%
px.histogram(
    metadata,
    x="dx_type",
    color="dx",
    hover_data=metadata.columns,
    title="Which diagnosing technique was used for which diagnosis?",
)

# %% [markdown]
# **Conclusion for ALVUC:**
# - For more information regarding each diagnosis type check (1)
# - Obviously `dx_type` is highly correlated to the actual diagnosis, and must not be used as input for any classifier
# - Since the `dx_type` column does not have any `NaN` values (see `metadata.isnull().sum()` above), each diagnosis is more than the opinion of a single physician

# %%
px.histogram(metadata, x="age", color="sex", barmode="group")

# %% [markdown]
# **Conclusion for ALVUC:** Since we're not planning on using `age` as a input feature, the distribution is not of high importance. Still, it appears that the data is a) rounded to 5 year bins b) distributed across the whole lifespan.

# %%
px.histogram(metadata, x="sex", color="dx")

# %% [markdown]
# **Conclusion for ALVUC:** The sex seems fairly balanced, with only a few samples with unknown sex. Again, we're not planning on using it as an input feature
#

# %%
px.histogram(metadata, x="localization", color="dx")

# %% [markdown]
# **Conclusion for ALVUC:**
# - There appears to be a correlation between some localizations and the classes of lesions, which might make this feature an interesting candidate to include as input for the machine learning model
# - However, some more research into the definitions of each category would be necessary. E.g., it's not immediately obvious, how `back` and `chest` are not part of `trunk`

# %% [markdown]
# ### Summary of 2.3 for ALVUC

# %% [markdown]
# - The dataset appears to be well balanced and with only minor amounts unknown/NaN samples.
# - `localization` could be a candidate as an additional input feature

# %% [markdown]
# ## 2.4 Data quality report
#
# Questions regarding completeness, missing values and potential errors have been answered in the sections above
