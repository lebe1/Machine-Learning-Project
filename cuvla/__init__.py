import os
from pathlib import Path


def get_project_root() -> Path:
    """
    Returns project root path
    Only works if alvuc package is installed via `pip install -e .` or if
    `CI_PROJECT_DIR` environment variable is set

    Returns: project root folder.
    """
    # Check if we're executed inside GitLab CI,
    # see https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
    ci_project_dir = os.getenv("CI_PROJECT_DIR")
    if ci_project_dir is None:
        # Not executed inside CI
        project_path = Path(__file__).parent.parent
    else:
        project_path = Path(ci_project_dir)

    return project_path
