import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from lime import lime_image
from loguru import logger
from skimage.segmentation import mark_boundaries
from skimage.transform import resize
from tensorflow.keras.preprocessing import image

import alvuc.gpu_utils
from alvuc.functions import config


alvuc.gpu_utils.mask_unused_gpus(count_gpus_unmasked=1, minimum_available_ram=9000)


# Define & Initialize Paths
dvc_paths = config.DvcPaths()

DATA_PREPROCESSED = dvc_paths.preprocessed_path
DATA_MODEL = dvc_paths.model_path
EXPLAIN_PATH = dvc_paths.explain_path
EXPLAIN_PATH.mkdir(exist_ok=True)


def load_image_to_explain(img_id: str) -> np.array:
    """
    Function to read a image based on imageID and return it as np array

    Args:
        img_id (str, required): ID of a image

    Returns:
        image_to_explain (np.array): loaded image as np format
    """
    image_to_explain = np.array((image.load_img(str(DATA_PREPROCESSED) + "/" + img_id + ".jpg")))

    return image_to_explain


class Lime:
    """
    Lime Explanation

    Args:
        class_selection (str): selected class name whose visualization is to be generated
        classes (dict): dictory of class indezes and class names
        model (tf.python.keras.engine.training.Model): Trained classifier to create predictions
        img_id (str): ID of a image
        num_samples (int, Default: 200): size of the neighborhood to learn the linear model
        hide_color (int, Default: 0): specifies whether relevant areas are to be marked or hidden in the visualization
        top_labels (int, Default: 0): specifies how many classes are to be output in the class_selection "Comparison".
        num_features (int, Default: 5): Number of features to be marked in the visualization
    """

    def __init__(
        self,
        class_selection: str,
        classes: dict,
        model: tf.python.keras.engine.training.Model,
        img_id: str,
        num_samples: int = 200,
        hide_color: int = 0,
        top_labels: int = 7,
        num_features: int = 5,
    ):
        self,
        self._class_selection = class_selection
        self._classes = classes
        self._model = model
        self._img_id = img_id
        self._num_samples = num_samples
        self._hide_color = hide_color
        self._top_labels = top_labels
        self._num_features = num_features

    def visualize_explanations(self, explanation: lime_image.ImageExplanation) -> None:
        """
        Function to visualize explanations of lime. These are displayed and stored in the explain output directory.

        Args:
            self (Rise-instance): Instance of a Rise object to read configuration parameters
            explanation (lime.lime_image.ImageExplanation, required): Instance of lime_image explanatioin that provides values of the explanation

        """

        # visualize all classes
        if self._class_selection == "Comparison":
            fig, axarr = plt.subplots(1, 7, figsize=(20, 5))
            i = 0
            for clazz in list(self._classes):
                axarr[i].set_title(f"{clazz}")
                temp, mask = explanation.get_image_and_mask(
                    explanation.top_labels[self._classes[clazz]],
                    positive_only=False,
                    num_features=self._num_features,
                    hide_rest=False,
                )
                plt.imshow(mark_boundaries(temp, mask))
                axarr[i].imshow(mark_boundaries(temp, mask))
                i += 1

            # save visualization
            plt.savefig(
                f"{str(EXPLAIN_PATH)}/LIME_{self._img_id}_{self._class_selection}.png", format="png"
            )

        # visualize a single class
        else:
            temp, mask = explanation.get_image_and_mask(
                explanation.top_labels[self._classes[self._class_selection]],
                positive_only=False,
                num_features=self._num_features,
                hide_rest=False,
            )
            plt.title(f"{self._class_selection}")
            plt.imshow(mark_boundaries(temp, mask))

            # save visualization
            plt.savefig(
                f"{str(EXPLAIN_PATH)}/LIME_{self._img_id}_{self._class_selection}.png", format="png"
            )

    def explain(self):
        """
        Generate explanations with lime and execute a interacitve widget to visualize the explanations
        LIME = Local Interpretable Modelagnostic Explanations
        LIME generates a local surrogate model and create explanations by the following steps:
        1. Get instance of a given image out of the data space, in our case a sample for which we would like an explanation
        2. Generate a dataset with perturbed instances around a given image
        3. Retrieve predictions for pertubed dataset using our black box model <model>
        4. Weight the pertubed dataset in relation to the proximity to the given image to explain
        5. Train a explainable weighted model

        Detailed information about LIME: [Marco Tulio Ribeiro, Sameer Singh, Carlos Guestrin (2016): "Why Should I Trust You?": Explaining the Predictions of Any Classifier](https://arxiv.org/abs/1602.04938)

        Args:
            self (Lime-instance): Instance of a Lime object to read configuration parameters

        """

        # initialze a lime image explainer
        # solves the pertubation of image data by highlighting and occlusion of image regions
        # searched with the superpixel algorithm "Lasso"
        explainer = lime_image.LimeImageExplainer()

        # generate explanations with lime
        explanation = explainer.explain_instance(
            load_image_to_explain(self._img_id),
            self._model.predict,
            hide_color=self._hide_color,
            top_labels=self._top_labels,
            num_samples=self._num_samples,
            num_features=self._num_features,
        )

        # visualize lime explanations
        self.visualize_explanations(explanation=explanation)

        logger.debug(f"LIME explanation for record {self._img_id} created.")


class Rise:
    """
    Rise Explanation

    Args:
        class_selection (str): selected class name whose visualization is to be generated
        classes (dict): dictory of class indezes and class names
        model (tf.python.keras.engine.training.Model): Trained classifier to create predictions
        img_id (str): ID of a image
        num_masks (int, Default: 2500): specifies how many masks should be created
        size_masks (int, Default: 5): masks are acquired by producing smaller masks of size size_masks
        propability_value (float [0,1], Default: 0.5): value of the propability with which the masks is to be filled initially
        batch_size (int, Default: 50): batch size with which the model predictions are to be queried
    """

    def __init__(
        self,
        class_selection: str,
        classes: dict,
        model: tf.python.keras.engine.training.Model,
        img_id: str,
        num_masks: int = 2500,
        size_masks: int = 5,
        propability_value: float = 0.5,
        batch_size: int = 50,
    ):
        self,
        self._num_masks = num_masks
        self._size_masks = size_masks
        self._propability_value = propability_value
        self._batch_size = batch_size
        self._class_selection = class_selection
        self._classes = classes
        self._model = model
        self._img_id = img_id

    def generate_masks(self) -> np.array:
        """
        Generate masks in the size of the image to explained.
        These masks are grids that are filled with values based on the RISE principle.

        Args:
            self (Rise-instance): Instance of a Rise object to read configuration parameters

        Returns:
            masks (np.array): masks to occlude the image to explain
        """

        assert (self._propability_value >= 0) & (
            self._propability_value <= 1
        ), "The given value is outside the valid range for a probability."

        # determine the required array size
        cell_size = np.ceil(
            np.array((self._model.input.shape[1], self._model.input.shape[2])) / self._size_masks
        )
        up_size = (self._size_masks + 1) * cell_size

        # generate arrays with random values of the size of the image
        grid = (
            np.random.rand(self._num_masks, self._size_masks, self._size_masks)
            < self._propability_value
        )
        grid = grid.astype("float32")

        # generate templates for the masks
        masks = np.empty(
            (self._num_masks, *(self._model.input.shape[1], self._model.input.shape[2]))
        )

        # finally insert the grids into the masks
        for i in range(self._num_masks):
            # Random shifts
            x = np.random.randint(0, cell_size[0])
            y = np.random.randint(0, cell_size[1])
            # Linear upsampling and cropping
            masks[i, :, :] = resize(grid[i], up_size, order=1, mode="reflect", anti_aliasing=False)[
                x : x + self._model.input.shape[1], y : y + self._model.input.shape[2]
            ]

        masks = masks.reshape(-1, *(self._model.input.shape[1], self._model.input.shape[2]), 1)

        return masks

    def generate_saliency_maps(self, masks: np.array) -> np.array:
        """
        Generate a saliency map for rise explanation visualizions.

        Args:
            self (Rise-instance): Instance of a Rise object to read configuration parameters
            masks (np.array, required): masks to overlay the image to explain, first axis correspondends to the number of masks

        Returns:
            saliency_maps (np.array): saliency maps to visualize importance of areas
        """

        # inialize a list to save predictions
        preds = []

        # Mask the image with generated masks
        masked = load_image_to_explain(self._img_id) * masks

        # require model predictions for masked images
        for i in range(0, masks.shape[0], self._batch_size):
            preds.append(self._model.predict(masked[i : min(i + self._batch_size, masks.shape[0])]))

        # concatenate predictions to a single array
        predictions = np.concatenate(preds)

        # generate saliency graphs based on the predictions
        saliency_maps = predictions.T.dot(masks.reshape(masks.shape[0], -1)).reshape(
            -1, *(self._model.input.shape[1], self._model.input.shape[2])
        )
        saliency_maps = saliency_maps / masks.shape[0] / self._propability_value

        return saliency_maps

    def visualize_explanations(
        self,
        saliency_maps: np.array,
    ) -> None:
        """
        Function to visualize explanations of rise. These are displayed and stored in the explain output directory.

        Args:
            self (Rise-instance): Instance of a Rise object to read configuration parameters
            saliency_maps (np.array, required): Saliency maps to visualize importance of areas

        """

        # visualize all classes
        if self._class_selection == "Comparison":
            fig, axarr = plt.subplots(1, len(self._classes), figsize=(20, 5))
            i = 0
            for clazz in list(self._classes):
                axarr[i].set_title(f"{list(self._classes)[i]}")
                axarr[i].imshow(load_image_to_explain(self._img_id))
                axarr[i].imshow(saliency_maps[self._classes[clazz]], cmap="jet", alpha=0.25)
                i += 1

            # save visualization
            plt.savefig(
                f"{str(EXPLAIN_PATH)}/RISE_{self._img_id}_{self._class_selection}.png", format="png"
            )

        # visualize a single class
        else:
            plt.imshow(load_image_to_explain(self._img_id))
            plt.imshow(
                saliency_maps[self._classes[self._class_selection]],
                cmap="jet",
                alpha=0.25,
            )
            plt.title(f"{self._class_selection}")
            plt.colorbar()

            # save visualization
            plt.savefig(
                f"{str(EXPLAIN_PATH)}/RISE_{self._img_id}_{self._class_selection}.png", format="png"
            )

        # display visualization
        plt.show()

    def explain(self) -> None:
        """
        Generate explanations with RISE and execute a interactive widget to visualize the explanations
        RISE = Randomized Input Sampling for Explanation
        RISE is a model agnostic approach to generate local explanations, based on the principle of occlusion, what happens in the following steps:
        1. Create random masks to cover the image areas (pixels) for a given sample
        2. Occlude the given image with one of the mask created from the previous step, by computing an element-wise multiplication
        3. Use the modified image to generate a new prediction for our black-box model <model>
        4. Repeat step 2-3 <um_masks> times, each time using a different mask taken from set of generated masks in step 1
        5. Combine results by computing the importance of each pixel to the resulting classification

            Detailed information about RISE: [Vitalii Petsiuk, Abir Das, Kate Saenko (2018): RISE: Randomized Input Sampling for Explanation of Black-box Models](https://arxiv.org/abs/1806.07421)

        Args:
            self (Rise-instance): Instance of a Rise object
        """

        masks = self.generate_masks()

        saliency_maps = self.generate_saliency_maps(masks)

        self.visualize_explanations(saliency_maps)

        logger.debug(f"RISE explanation for record {self._img_id} created.")
