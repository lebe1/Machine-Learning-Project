# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.6.0
#   kernelspec:
#     display_name: alvuc
#     language: python
#     name: alvuc
# ---
# %% [markdown]
# ##### 6. Explain
#
# In this notebook the predictions of the trained model can be explained.
# %%
# %load_ext autoreload
# %autoreload 2
import pandas as pd
import tensorflow as tf
import yaml
from loguru import logger

import alvuc.explain.explain_toolbox
import alvuc.gpu_utils
import alvuc.util
from alvuc.functions import config
from alvuc.util import AlvucDataGenerator

alvuc.gpu_utils.mask_unused_gpus(count_gpus_unmasked=1, minimum_available_ram=9000)


# Define & Initialize Paths
dvc_paths = config.DvcPaths()

DATA_PREPROCESSED = dvc_paths.preprocessed_path
DATA_MODEL = dvc_paths.model_path
EXPLAIN_PATH = dvc_paths.explain_path
EXPLAIN_PATH.mkdir(exist_ok=True)

# Load Params
with open(dvc_paths.params_path) as file:
    params = yaml.safe_load(file)
EXPLAINABILITY_METHOD = params["EXPLAIN"]["EXPLAINABILITY_METHOD"]
CLASS_SELECTION = params["EXPLAIN"]["CLASS_SELECTION"]
RECORDS_TO_EXPLAIN = params["EXPLAIN"]["RECORDS_TO_EXPLAIN"]
IMG_SIZE_X = params["PREPROCESS"]["IMG_SIZE_X"]
IMG_SIZE_Y = params["PREPROCESS"]["IMG_SIZE_Y"]

# Load Commplete Dataset
metadata_df = pd.read_csv(dvc_paths.raw_data_path / "HAM10000_metadata.csv")

# Load Testdata
test_df = pd.read_csv(dvc_paths.train_test_split_path / "test.csv", index_col=0)

# Load Traindata
train_df = metadata_df[~metadata_df["image_id"].isin(test_df["image_id"])]

# append path to complete dataset
metadata_df["path_preprocessed"] = metadata_df.apply(
    lambda row: str(dvc_paths.preprocessed_path / (row["image_id"] + ".jpg")), axis=1
)

# Split the single string containing a list of classes and strip any remaining ` `
clazzes = [clazz.strip() for clazz in params["LABELS"].split(",")]
BATCH_SIZE = 1

generator_params = {
    "x_col": "path_preprocessed",
    "y_col": "dx",
    "batch_size": BATCH_SIZE,
    "classes": clazzes,
}

sample_generator_instance = AlvucDataGenerator()
sample_generator, STEP_SIZE_TRAIN, _ = sample_generator_instance.flow_from_metadata_df(
    metadata_df, target_size_x=IMG_SIZE_X, target_size_y=IMG_SIZE_Y, **generator_params
)

# Get a dictionary with all classes from the test generator
# --> {"akiec": 0, "bcc": 1, "bkl": 2}
class_labels_dict = sample_generator.class_indices

# load model to explain
# TODO: implement custom model selection
model = tf.keras.models.load_model(DATA_MODEL / "model.h5", compile=True)

if __name__ == "__main__":

    records_to_explain = RECORDS_TO_EXPLAIN

    if "test" in records_to_explain:
        # create list of test records
        records_to_explain = list(test_df["image_id"])

    for img_id in records_to_explain:
        # explain images with selected model interpretability method

        if EXPLAINABILITY_METHOD == "lime":

            from alvuc.explain.explain_toolbox import Lime

            alvuc_lime = Lime(
                classes=class_labels_dict,
                class_selection=CLASS_SELECTION,
                model=model,
                img_id=img_id,
            )

            alvuc_lime.explain()

        elif EXPLAINABILITY_METHOD == "rise":

            from alvuc.explain.explain_toolbox import Rise

            alvuc_rise = Rise(
                classes=class_labels_dict,
                class_selection=CLASS_SELECTION,
                model=model,
                img_id=img_id,
            )

            alvuc_rise.explain()

        elif EXPLAINABILITY_METHOD == "all":

            from alvuc.explain.explain_toolbox import Lime
            from alvuc.explain.explain_toolbox import Rise

            alvuc_lime = Lime(
                classes=class_labels_dict,
                class_selection=CLASS_SELECTION,
                model=model,
                img_id=img_id,
            )

            alvuc_rise = Rise(
                classes=class_labels_dict,
                class_selection=CLASS_SELECTION,
                model=model,
                img_id=img_id,
            )

            alvuc_lime.explain()
            alvuc_rise.explain()

        else:

            logger.debug(f"No explanation for record {img_id} created -- pass.")
