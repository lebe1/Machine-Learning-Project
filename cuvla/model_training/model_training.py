import random as python_random
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd
import tensorflow as tf
import tensorflow.keras as keras
import yaml
from loguru import logger
from tensorflow.keras.callbacks import ReduceLROnPlateau
from tensorflow.keras.metrics import categorical_accuracy
from tensorflow.keras.optimizers import Adam

import alvuc.gpu_utils
from alvuc.functions import config
from alvuc.functions.create_image_paths_in_df import create_image_paths_in_df
from alvuc.util import AlvucDataGenerator
from alvuc.util.mlflow_log_model_params import mlflow_log_model_params

dvc_paths = config.DvcPaths()

with open(dvc_paths.params_path) as file:
    params = yaml.safe_load(file)
RANDOM_STATE = params["RANDOM_STATE"]

np.random.seed(RANDOM_STATE)
python_random.seed(RANDOM_STATE)
tf.random.set_seed(RANDOM_STATE)


alvuc.gpu_utils.mask_unused_gpus(count_gpus_unmasked=1, minimum_available_ram=9000)


def model_training(
    train_df: pd.DataFrame, test_df: pd.DataFrame, params: dict, model_path: Path
) -> None:
    """
    This script defines the stage for training our model. After executing this script, we get a
    new trained neural network model.

    Args:
        train_df (pd.DataFrame): DataFrame containing the paths and targets of the images which are used for training
        test_df (pd.DataFrame): DataFrame containing the paths and targets of the images which are used for testing/validating
        params (dict): parameters from params.yaml
        model_path (Path): Path of the model to be saved
    """

    # Read parameters required for model training from params dict
    IMG_SIZE_X = params["PREPROCESS"]["IMG_SIZE_X"]
    IMG_SIZE_Y = params["PREPROCESS"]["IMG_SIZE_Y"]
    BATCH_SIZE = params["MODEL_TRAINING"]["BATCH_SIZE"]
    EPOCHS = params["MODEL_TRAINING"]["EPOCHS"]
    LEARNING_RATE = params["MODEL_TRAINING"]["LEARNING_RATE"]
    MODEL_ARCH = params["MODEL_TRAINING"]["MODEL_ARCH"]
    AUGMENTATION = params["MODEL_TRAINING"]["AUGMENTATION"]
    # Split the single string containing a list of classes and strip any remaining ` `
    clazzes = [clazz.strip() for clazz in params["LABELS"].split(",")]

    # ## Model building
    if MODEL_ARCH == "Marsh":
        from alvuc.model_training.model import Marsh

        model = Marsh.create_model(LEARNING_RATE)
    elif MODEL_ARCH == "Siddharta":
        from alvuc.model_training.model import Siddharta

        model = Siddharta.create_model(LEARNING_RATE)
    elif MODEL_ARCH == "Inception":
        from alvuc.model_training.model import Inception

        model = Inception.create_model(LEARNING_RATE)
    elif MODEL_ARCH == "DenseNet201":
        from alvuc.model_training.model import DenseNet201

        model = DenseNet201.create_model(LEARNING_RATE)
    elif MODEL_ARCH == "ResNet50":
        from alvuc.model_training.model import ResNet50

        model = ResNet50.create_model(LEARNING_RATE)
    elif MODEL_ARCH == "MobileNetV2":
        from alvuc.model_training.model import MobileNetV2

        model = MobileNetV2.create_model(LEARNING_RATE)
    elif MODEL_ARCH == "EfficientNetB0":
        from alvuc.model_training.model import EfficientNetB0

        model = EfficientNetB0.create_model(LEARNING_RATE)
    else:
        raise ValueError("You forgot your model architecture")

    # Keep this debug logging statement for a quick overview of the model architecture
    model.summary(print_fn=logger.debug)

    # Function for MLFlow to handle logging of training parameters if MLFlow active, else
    # write empty mlflow_run_id file
    mlflow_run_id = mlflow_log_model_params(train, test, params)

    with open(model_path / "mlflow_run_id", "w") as file:
        logger.debug(
            f"Writing mlflow run id {mlflow_run_id} to {(model_path / 'mlflow_run_id').absolute()}"
        )
        file.writelines(mlflow_run_id)

    if len(train) == 0:
        # TODO this case has grown quite long and duplicates lots of calls of the main script. Refactor it
        logger.warning(
            "No training data available, storing untrained model and exiting model training"
        )
        model.save(model_path / "model.h5")
        pd.DataFrame(columns=["NO_TRAINING_HAPPENED"]).to_csv(model_path / "history.csv")
        # TODO remove this, once tensorboard integration has been removed. Currently its necessary, because dvc expects a directory to be present
        (model_path / "tensorboard_logs").mkdir(exist_ok=True)
        # TODO Check whether exit is the correct method to call here
        exit()

    # Set a learning rate annealer
    learning_rate_reduction = ReduceLROnPlateau(
        monitor="val_accuracy", patience=3, verbose=1, factor=0.5, min_lr=0.00001
    )

    # Configuring Tensorboard
    tensorboard_log_dir = model_path / "tensorboard_logs" / datetime.now().strftime("%Y%m%d-%H%M%S")
    file_writer = tf.summary.create_file_writer(str(tensorboard_log_dir))
    file_writer.set_as_default()

    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=tensorboard_log_dir)

    # Create instances for train_generator and test_generator
    if AUGMENTATION:
        augmentation_params = {
            "rotation_range": 180,
            "width_shift_range": 0.1,
            "height_shift_range": 0.1,
            "zoom_range": 0.1,
            "horizontal_flip": True,
            "vertical_flip": True,
            "fill_mode": "nearest",
        }
        train_generator_instance = AlvucDataGenerator(**augmentation_params)
    else:
        train_generator_instance = AlvucDataGenerator()

    # Set parameters for train and test generators
    generator_params = {
        "x_col": "img_path",
        "y_col": "dx",
        "batch_size": BATCH_SIZE,
        "classes": clazzes,
    }

    train_generator, STEP_SIZE_TRAIN, _ = train_generator_instance.flow_from_metadata_df(
        train, target_size_x=IMG_SIZE_X, target_size_y=IMG_SIZE_Y, **generator_params
    )

    test_generator_instance = AlvucDataGenerator()
    test_generator, STEP_SIZE_TEST, _ = test_generator_instance.flow_from_metadata_df(
        test, target_size_x=IMG_SIZE_X, target_size_y=IMG_SIZE_Y, **generator_params
    )

    if MODEL_ARCH == "Inception":

        model_history = model.fit(
            train_generator,
            steps_per_epoch=STEP_SIZE_TRAIN,
            validation_data=test_generator,
            validation_steps=STEP_SIZE_TEST,
            epochs=5,
            verbose=2,
            callbacks=[learning_rate_reduction, tensorboard_callback],
        )

        # After generic feature extraction:
        # trying to fine tune the model by setting all layers starting from Conv5_* block to trainable=True
        for layer, _ in list(model._get_trainable_state().items())[249:]:
            layer.trainable = True

        optimizer = Adam(
            lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=True
        )
        model.compile(
            optimizer=optimizer,
            loss="categorical_crossentropy",
            metrics=[categorical_accuracy],
        )

    if MODEL_ARCH == "DenseNet201":

        model_history = model.fit(
            train_generator,
            steps_per_epoch=STEP_SIZE_TRAIN,
            validation_data=test_generator,
            validation_steps=STEP_SIZE_TEST,
            epochs=5,
            verbose=2,
            callbacks=[learning_rate_reduction, tensorboard_callback],
        )

        # After feature extraction: trying to fine tune the model by setting all layers starting from Conv5_* block to trainable=True
        # Variation: Conv4_* block would be starting from index [141:], Conv5_* block from [481:]
        for layer, _ in list(model._get_trainable_state().items())[481:]:
            layer.trainable = True

        optimizer = Adam(
            lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=True
        )
        model.compile(
            optimizer=optimizer,
            loss="categorical_crossentropy",
            metrics=[categorical_accuracy],
        )

    if test_generator.n < BATCH_SIZE:
        raise Exception(
            f"The selected test size is too small for the selected BATCH_SIZE of {BATCH_SIZE}"
        )

    model_history = model.fit(
        train_generator,
        steps_per_epoch=STEP_SIZE_TRAIN,
        validation_data=test_generator,
        validation_steps=STEP_SIZE_TEST,
        epochs=EPOCHS,
        verbose=2,
        callbacks=[learning_rate_reduction, tensorboard_callback],
    )

    # Save Model & Training History
    model.save(model_path / "model.h5")
    pd.DataFrame(model_history.history).to_csv(model_path / "history.csv")


if __name__ == "__main__":
    # Define & Initialize Paths
    dvc_paths = config.DvcPaths()
    MODEL_PATH = dvc_paths.model_path
    MODEL_PATH.mkdir(parents=True, exist_ok=True)

    with open(dvc_paths.params_path) as file:
        params = yaml.safe_load(file)

    train_raw = pd.read_csv(dvc_paths.train_test_split_path / "train.csv", index_col=0)
    test_raw = pd.read_csv(dvc_paths.train_test_split_path / "test.csv", index_col=0)

    # Create the path to the preprocessed images. DataGenerator later expects strings => cast to string
    train = create_image_paths_in_df(train_raw, dvc_paths.preprocessed_path)
    test = create_image_paths_in_df(test_raw, dvc_paths.preprocessed_path)
    model_training(train, test, params, MODEL_PATH)
