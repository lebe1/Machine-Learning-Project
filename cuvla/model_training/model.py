import tensorflow as tf
import tensorflow.keras as keras
import yaml
from loguru import logger
from tensorflow.keras import backend as K
from tensorflow.keras import optimizers
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import GlobalMaxPooling2D
from tensorflow.keras.layers import MaxPool2D
from tensorflow.keras.metrics import categorical_accuracy
from tensorflow.keras.metrics import top_k_categorical_accuracy
from tensorflow.keras.models import Model
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam

from alvuc.functions import config

dvc_paths = config.DvcPaths()
with open(dvc_paths.params_path) as file:
    params = yaml.safe_load(file)
IMG_SIZE_X = params["PREPROCESS"]["IMG_SIZE_X"]
IMG_SIZE_Y = params["PREPROCESS"]["IMG_SIZE_Y"]
TRAINING = params["MODEL_TRAINING"]["TRAINING"]
BN_TRAINING = params["MODEL_TRAINING"]["BN_TRAINING"]


class Marsh:

    """
    Following the kaggle kernel of Marsh, this kernel uses transfer leraning.
    Source: https://www.kaggle.com/vbookshelf/skin-lesion-analyzer-tensorflow-js-web-app
    """

    def create_model(LEARNING_RATE):

        # load mobile net model
        input_tensor = tf.keras.layers.Input(shape=(IMG_SIZE_Y, IMG_SIZE_X, 3))

        model = tf.keras.applications.mobilenet.MobileNet(
            input_tensor=input_tensor, include_top=True
        )

        # Exclude the last 5 layers of the above model.
        # This will include all layers up to and including global_average_pooling2d_1
        x = model.layers[-6].output

        # Create a new dense layer for predictions
        # 7 corresponds to the number of classes
        x = Dropout(0.25)(x)
        predictions = Dense(7, activation="softmax")(x)

        # inputs=mobile.input selects the input layer, outputs=predictions refers to the
        # dense layer we created above.
        model = Model(inputs=model.input, outputs=predictions)

        # modify mobile net model
        """
        Unfreeze layers improved the original marsh-kernel, why the following code should no longer be executed.

        # We need to choose how many layers we actually want to be trained.
        # Here we are freezing the weights of all layers except the
        # last 23 layers in the new model.
        # The last 23 layers of the model will be trained.

        for layer in model.layers[:-23]:
            layer.trainable = False
        """

        # Define Top2 and Top3 Accuracy
        def top_3_accuracy(y_true, y_pred):
            return top_k_categorical_accuracy(y_true, y_pred, k=3)

        def top_2_accuracy(y_true, y_pred):
            return top_k_categorical_accuracy(y_true, y_pred, k=2)

        # Define the optimizer
        optimizer = optimizers.Adam(lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, amsgrad=False)

        # Compile the model
        model.compile(
            optimizer=optimizer, loss="categorical_crossentropy", metrics=[categorical_accuracy]
        )

        return model


class Siddharta:

    """
    Following the kaggle kernel of Manu Siddharta. This kernel does not require data augmentation
    and is therefore interesting for active learning.
    Source: https://www.kaggle.com/sid321axn/step-wise-approach-cnn-model-77-0344-accuracy
    """

    def create_model(LEARNING_RATE):

        # Set the CNN model
        # my CNN architechture is In -> [[Conv2D->relu]*2 -> MaxPool2D -> Dropout]*2 -> Flatten -> Dense -> Dropout -> Out
        input_shape = (IMG_SIZE_Y, IMG_SIZE_X, 3)
        num_classes = 7

        model = Sequential()

        model.add(
            Conv2D(
                32, kernel_size=(3, 3), activation="relu", padding="Same", input_shape=input_shape
            )
        )
        model.add(
            Conv2D(
                32,
                kernel_size=(3, 3),
                activation="relu",
                padding="Same",
            )
        )
        model.add(MaxPool2D(pool_size=(2, 2)))
        model.add(Dropout(0.25))

        model.add(Conv2D(64, (3, 3), activation="relu", padding="Same"))
        model.add(Conv2D(64, (3, 3), activation="relu", padding="Same"))
        model.add(MaxPool2D(pool_size=(2, 2)))
        model.add(Dropout(0.40))

        model.add(Flatten())
        model.add(Dense(128, activation="relu"))
        model.add(Dropout(0.5))
        model.add(Dense(num_classes, activation="softmax"))

        # Define the optimizer
        optimizer = optimizers.Adam(lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, amsgrad=False)

        # Compile the model
        model.compile(
            optimizer=optimizer, loss="categorical_crossentropy", metrics=[categorical_accuracy]
        )

        return model


class Inception:

    """
    Trying transfer learning approach with InceptionV3 model
    """

    def create_model(LEARNING_RATE):
        # Load InceptionV3 model
        transfer_model = keras.applications.inception_v3.InceptionV3(
            input_shape=(IMG_SIZE_Y, IMG_SIZE_X, 3), weights="imagenet", include_top=False
        )

        for layer, _ in transfer_model._get_trainable_state().items():
            if hasattr(layer, "moving_mean") and hasattr(layer, "moving_variance"):
                layer.trainable = True
                K.eval(K.update(layer.moving_mean, K.zeros_like(layer.moving_mean)))
                K.eval(K.update(layer.moving_variance, K.zeros_like(layer.moving_variance)))
            else:
                layer.trainable = False

        last_layer = transfer_model.get_layer("mixed10")
        last_layer_output = last_layer.output

        x = GlobalMaxPooling2D()(last_layer_output)
        x = Dense(512, activation="relu")(x)
        x = Dropout(0.3)(x)
        predictions = Dense(7, activation="softmax")(x)

        model = Model(inputs=transfer_model.input, outputs=predictions, name="Inception_Alvuc")

        optimizer = Adam(
            lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=True
        )
        model.compile(
            optimizer=optimizer, loss="categorical_crossentropy", metrics=[categorical_accuracy]
        )

        return model


class DenseNet201:

    """
    Trying transfer learning approach with DenseNet201 model
    """

    def create_model(LEARNING_RATE):
        # Load model
        # input_tensor = tf.keras.layers.Input(shape=(150, 200, 3))
        transfer_model = tf.keras.applications.densenet.DenseNet201(
            input_shape=(IMG_SIZE_Y, IMG_SIZE_X, 3), weights="imagenet", include_top=False
        )

        # Set all batch normalization layers to trainable=True, everything else to trainable=False
        # https://arxiv.org/abs/1502.03167
        for layer, _ in transfer_model._get_trainable_state().items():
            if hasattr(layer, "moving_mean") and hasattr(layer, "moving_variance"):
                layer.trainable = True
                K.eval(K.update(layer.moving_mean, K.zeros_like(layer.moving_mean)))
                K.eval(K.update(layer.moving_variance, K.zeros_like(layer.moving_variance)))
            else:
                layer.trainable = False

        last_layer = transfer_model.get_layer("relu")
        last_layer_output = last_layer.output
        x = GlobalMaxPooling2D()(last_layer_output)
        x = Dense(512, activation="relu")(x)
        x = Dropout(0.5)(x)
        predictions = Dense(7, activation="softmax")(x)

        model = Model(transfer_model.input, predictions)
        optimizer = Adam(
            lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=True
        )

        model.compile(
            loss="categorical_crossentropy", optimizer=optimizer, metrics=[categorical_accuracy]
        )

        return model


class EfficientNetB0:

    """
    Trying transfer learning approach with EfficientNetB0 model
    """

    def create_model(LEARNING_RATE):
        num_classes = 7

        pretrained_kwargs = {
            "input_shape": (IMG_SIZE_Y, IMG_SIZE_X, 3),
            "include_top": False,
            "pooling": "avg",
            "weights": "imagenet",
        }

        pretrained_base = tf.keras.applications.EfficientNetB0(**pretrained_kwargs)

        if TRAINING:
            logger.info(f"Training every layer of EfficientNetB0 set to {TRAINING}")
            pretrained_base.trainable = True
        elif BN_TRAINING:
            logger.info(
                f"Training every Batch Normalization layer of EfficientNetB0 set to {BN_TRAINING}"
            )
            for layer, _ in pretrained_base._get_trainable_state().items():
                if hasattr(layer, "moving_mean") and hasattr(layer, "moving_variance"):
                    layer.trainable = True
                    K.eval(K.update(layer.moving_mean, K.zeros_like(layer.moving_mean)))
                    K.eval(K.update(layer.moving_variance, K.zeros_like(layer.moving_variance)))
                else:
                    layer.trainable = False
        else:
            logger.info(
                f"Freezing every layer of EfficientNetB0. Training set to {TRAINING} and BN_Training set to {BN_TRAINING}"
            )
            pretrained_base.trainable = False

        # Base
        x_last_layer = pretrained_base.output

        # Head
        x = Dense(512, activation="relu")(x_last_layer)
        x = Dropout(0.3)(x)
        outputs = keras.layers.Dense(num_classes, activation="softmax")(x)

        model = keras.Model(pretrained_base.input, outputs)

        model.summary()

        # Define Top2 and Top3 Accuracy
        def top_3_accuracy(y_true, y_pred):
            return top_k_categorical_accuracy(y_true, y_pred, k=3)

        def top_2_accuracy(y_true, y_pred):
            return top_k_categorical_accuracy(y_true, y_pred, k=2)

        optimizer = Adam(
            lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=True
        )

        model.compile(
            optimizer=optimizer, loss="categorical_crossentropy", metrics=[categorical_accuracy]
        )

        return model


class ResNet50:

    """
    Trying transfer learning approach with ResNet50 model
    """

    def create_model(LEARNING_RATE):
        num_classes = 7

        pretrained_kwargs = {
            "input_shape": (IMG_SIZE_Y, IMG_SIZE_X, 3),
            "include_top": False,
            "pooling": "avg",
            "weights": "imagenet",
        }

        pretrained_base = tf.keras.applications.ResNet50(**pretrained_kwargs)

        if TRAINING:
            logger.info(f"Training every layer of ResNet50 set to {TRAINING}")
            pretrained_base.trainable = True
        elif BN_TRAINING:
            logger.info(
                f"Training every Batch Normalization layer of ResNet50 set to {BN_TRAINING}"
            )
            for layer, _ in pretrained_base._get_trainable_state().items():
                if hasattr(layer, "moving_mean") and hasattr(layer, "moving_variance"):
                    layer.trainable = True
                    K.eval(K.update(layer.moving_mean, K.zeros_like(layer.moving_mean)))
                    K.eval(K.update(layer.moving_variance, K.zeros_like(layer.moving_variance)))
                else:
                    layer.trainable = False
        else:
            logger.info(
                f"Freezing every layer of ResNet50. Training set to {TRAINING} and BN_Training set to {BN_TRAINING}"
            )
            pretrained_base.trainable = False

        # Base
        x_last_layer = pretrained_base.output

        # Head
        x = Dense(512, activation="relu")(x_last_layer)
        x = Dropout(0.3)(x)
        outputs = keras.layers.Dense(num_classes, activation="softmax")(x)

        model = keras.Model(pretrained_base.input, outputs)

        model.summary()

        # Define Top2 and Top3 Accuracy
        def top_3_accuracy(y_true, y_pred):
            return top_k_categorical_accuracy(y_true, y_pred, k=3)

        def top_2_accuracy(y_true, y_pred):
            return top_k_categorical_accuracy(y_true, y_pred, k=2)

        optimizer = Adam(
            lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=True
        )

        model.compile(
            optimizer=optimizer, loss="categorical_crossentropy", metrics=[categorical_accuracy]
        )

        return model


class MobileNetV2:

    """
    Trying transfer learning approach with MobileNetV2 model
    """

    def create_model(LEARNING_RATE):
        num_classes = 7

        pretrained_kwargs = {
            "input_shape": (IMG_SIZE_Y, IMG_SIZE_X, 3),
            "include_top": False,
            "pooling": "avg",
            "weights": "imagenet",
        }

        pretrained_base = tf.keras.applications.MobileNetV2(**pretrained_kwargs)

        if TRAINING:
            logger.info(f"Training every layer of MobileNetV2 set to {TRAINING}")
            pretrained_base.trainable = True
        elif BN_TRAINING:
            logger.info(
                f"Training every Batch Normalization layer of MobileNetV2 set to {BN_TRAINING}"
            )
            for layer, _ in pretrained_base._get_trainable_state().items():
                if hasattr(layer, "moving_mean") and hasattr(layer, "moving_variance"):
                    layer.trainable = True
                    K.eval(K.update(layer.moving_mean, K.zeros_like(layer.moving_mean)))
                    K.eval(K.update(layer.moving_variance, K.zeros_like(layer.moving_variance)))
                else:
                    layer.trainable = False
        else:
            logger.info(
                f"Freezing every layer of MobileNetV2. Training set to {TRAINING} and BN_Training set to {BN_TRAINING}"
            )
            pretrained_base.trainable = False

        # Base
        x_last_layer = pretrained_base.output

        # Head
        x = Dense(512, activation="relu")(x_last_layer)
        x = Dropout(0.3)(x)
        outputs = keras.layers.Dense(num_classes, activation="softmax")(x)

        model = keras.Model(pretrained_base.input, outputs)

        model.summary()

        # Define Top2 and Top3 Accuracy
        def top_3_accuracy(y_true, y_pred):
            return top_k_categorical_accuracy(y_true, y_pred, k=3)

        def top_2_accuracy(y_true, y_pred):
            return top_k_categorical_accuracy(y_true, y_pred, k=2)

        optimizer = Adam(
            lr=LEARNING_RATE, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=True
        )

        model.compile(
            optimizer=optimizer, loss="categorical_crossentropy", metrics=[categorical_accuracy]
        )

        return model
