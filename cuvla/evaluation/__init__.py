import itertools

import matplotlib.pyplot as plt
import numpy as np


def plot_model_history(model_history):
    """
    Function to plot model's validation loss and validation accuracy
    by Manu Siddharta
    """
    fig, axs = plt.subplots(1, 2, figsize=(15, 5))
    # summarize history for accuracy
    axs[0].plot(
        range(1, len(model_history.history["accuracy"]) + 1), model_history.history["accuracy"]
    )
    axs[0].plot(
        range(1, len(model_history.history["val_accuracy"]) + 1),
        model_history.history["val_accuracy"],
    )
    axs[0].set_title("Model Accuracy")
    axs[0].set_ylabel("Accuracy")
    axs[0].set_xlabel("Epoch")
    axs[0].set_xticks(
        np.arange(1, len(model_history.history["accuracy"]) + 1),
        len(model_history.history["accuracy"]) / 10,
    )
    axs[0].legend(["train", "val"], loc="best")
    # summarize history for loss
    axs[1].plot(range(1, len(model_history.history["loss"]) + 1), model_history.history["loss"])
    axs[1].plot(
        range(1, len(model_history.history["val_loss"]) + 1), model_history.history["val_loss"]
    )
    axs[1].set_title("Model Loss")
    axs[1].set_ylabel("Loss")
    axs[1].set_xlabel("Epoch")
    axs[1].set_xticks(
        np.arange(1, len(model_history.history["loss"]) + 1),
        len(model_history.history["loss"]) / 10,
    )
    axs[1].legend(["train", "val"], loc="best")
    plt.show()


def plot_confusion_matrix(cm, classes, title="Confusion matrix", cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    Source: Scikit Learn
    """

    plt.imshow(cm, interpolation="nearest", cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = ".2f" if cm.dtype == "float64" else "d"
    thresh = cm.max() / 2.0
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(
            j,
            i,
            format(cm[i, j], fmt),
            horizontalalignment="center",
            color="white" if cm[i, j] > thresh else "black",
        )

    plt.ylabel("True label")
    plt.xlabel("Predicted label")
    plt.tight_layout()
