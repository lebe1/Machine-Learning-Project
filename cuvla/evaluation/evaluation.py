# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.6.0
#   kernelspec:
#     display_name: alvuc
#     language: python
#     name: alvuc
# ---
# %% [markdown]
# # 5. Evaluation
#
# In this notebook the trained model is evaluated on the testdata.
# %%
# %load_ext autoreload
# %autoreload 2
import json
import os

import numpy as np
import pandas as pd
import tensorflow as tf
import yaml
from loguru import logger
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

import alvuc.evaluation
import alvuc.gpu_utils
from alvuc.functions import config
from alvuc.util import AlvucDataGenerator
from alvuc.util import flatten_dict
from alvuc.util.mlflow_setup import mlflow_setup


alvuc.gpu_utils.mask_unused_gpus(count_gpus_unmasked=1, minimum_available_ram=9000)


def evaluation():
    """
    This script defines the stage for evaluating our model training. the resulting output files
    `confusionMatrix.csv` and eval.json can be used to assess the quality of our trained network.
    """

    # Define & initialize paths
    dvc_paths = config.DvcPaths()

    DATA_PREPROCESSED = dvc_paths.preprocessed_path
    DATA_MODEL = dvc_paths.model_path
    DATA_EVALUATION = dvc_paths.eval_path
    DATA_EVALUATION.mkdir(parents=True, exist_ok=True)

    with open(dvc_paths.params_path) as file:
        params = yaml.safe_load(file)
    IMG_SIZE_X = params["PREPROCESS"]["IMG_SIZE_X"]
    IMG_SIZE_Y = params["PREPROCESS"]["IMG_SIZE_Y"]

    # %% [markdown]
    # ### Load Testdata
    # %%
    # load test metadata
    test_df = pd.read_csv(dvc_paths.train_test_split_path / "test.csv", index_col=0)

    # NOTE: Skip script if there is no testset
    if test_df.empty:
        logger.info("There were no testset, creating empty eval.json")
        with open(DATA_EVALUATION / "eval.json", "w+") as json_file:
            json.dump(["No"], json_file)
        logger.info("There were no testdata, creating empty confusionMatrix.csv")
        csv = pd.DataFrame()
        csv.to_csv(DATA_EVALUATION / "confusionMatrix.csv", index=False)
        exit()

    test_df["img_path"] = test_df.apply(
        lambda row: str(DATA_PREPROCESSED / (row["image_id"] + ".jpg")), axis=1
    )

    # %%

    # Split the single string containing a list of classes and strip any remaining ` `
    clazzes = [clazz.strip() for clazz in params["LABELS"].split(",")]

    datagenerator_params = {
        "x_col": "img_path",
        "y_col": "dx",
        "classes": clazzes,
        "batch_size": 1,
    }

    datagenerator_instance = AlvucDataGenerator()
    (
        test_generator,
        batches_per_epoch,
        y_true_indices,
    ) = datagenerator_instance.flow_from_metadata_df(
        test_df, target_size_x=IMG_SIZE_X, target_size_y=IMG_SIZE_Y, **datagenerator_params
    )

    # %% [markdown]
    # ### Load Model
    # %%
    model = tf.keras.models.load_model(DATA_MODEL / "model.h5", compile=True)

    # %%
    # Get predictions for the test_df
    y_pred_proba = model.predict(test_generator, steps=batches_per_epoch)
    # %%
    # Get the index of the maximum prediction probability per "row"
    y_pred_indices = np.argmax(y_pred_proba, axis=1)
    y_pred_indices = y_pred_indices.tolist()

    # %%
    loss, __ = model.evaluate(test_generator, steps=batches_per_epoch, verbose=2)

    # %%
    # Get a dictionary with all classes from the test generator
    # --> {"akiec": 0, "bcc": 1, "bkl": 2}
    class_labels_dict = test_generator.class_indices

    # %%
    # Swap keys and values for easier lookup of class names by index
    # --> {0: "akiec", 1: "bcc", 2: "bkl"}
    class_labels_dict = {v: k for k, v in class_labels_dict.items()}

    # %% Write class names into y_pred and y_true
    y_pred = [class_labels_dict[k] for k in y_pred_indices]
    y_true = [class_labels_dict[k] for k in y_true_indices]

    # %%
    class_report = classification_report(y_true, y_pred, output_dict=True)

    # %%
    conf_m_not_normalized = confusion_matrix(y_true, y_pred, normalize=None)

    # %%
    conf_m_normalized = confusion_matrix(y_true, y_pred, normalize="true")

    # %% [markdown]
    # ## Save Metrics

    # %%
    # crate a metrics dictionary
    eval_results = {
        "loss": loss,
        "class_report": class_report,
        "confusion_matrix_not_normalized": conf_m_not_normalized.tolist(),
        "confusion_matrix_normalized": conf_m_normalized.tolist(),
    }
    # export the data for the confusion matrix
    csv = pd.DataFrame()
    csv["pred_classes"] = y_pred
    csv["true_classes"] = y_true
    csv.to_csv(DATA_EVALUATION / "confusionMatrix.csv", index=False)

    # %%
    # Evaluate current queryset: count labels from records which are selected in the current queryset

    metadata_df = pd.read_csv(dvc_paths.raw_data_path / "HAM10000_metadata.csv")
    queryset_csv_path = dvc_paths.query_path / "queryset.csv"
    queryset_df = pd.read_csv(queryset_csv_path)

    queryset_df = metadata_df[metadata_df["image_id"].isin(queryset_df["image_id"])]

    queryset_report = {"qs_support": queryset_df["dx"].value_counts().to_dict()}
    # %%
    # Configure MLFlow
    if os.getenv("MLFLOW_ACTIVE", "False") == "True":

        with open(DATA_MODEL / "mlflow_run_id") as file:
            mlflow_run_id = file.readline()

        # log classification report and queryset report to mlflow, other metrics in eval_results are lists wich cant be logged by mlflow
        # Combining 2 dicts: https://stackoverflow.com/a/26853961
        mlflow_metrics = {
            **flatten_dict(class_report),
            **flatten_dict(queryset_report),
        }

        mlflow_setup(
            run_id=mlflow_run_id,
            experiment_name=os.getenv("MLFLOW_EXPERIMENT_NAME", "Default"),
            mlflow_metrics=mlflow_metrics,
        )
    # %%

    # save evaluation metrics to a json file, which is managed by DVC
    with open(DATA_EVALUATION / "eval.json", "w+") as json_file:
        json.dump(eval_results, json_file)

    # %% [markdown]
    # ## Load & Display Metrics

    # %% [markdown]
    # Although the following section of the notebook won't be visible when run during the CI-Pipeline, executing it through CI is relevant to ensure that the methods run without error.
    #
    # This section is then intended to be executed manually after `git checkout && dvc checkout` of an experiment

    # %%
    with open(DATA_EVALUATION / "eval.json", "r") as json_file:
        # different var name than `eval_results` to ensure proper loading
        eval_metrics = json.load(json_file)

    # %% [markdown]
    # The `accuracy` column is a bit weird, in the text form of the classification report it's a single value in the `f1-score` row, pandas broadcasts it across the whole column.

    # %%
    eval_metrics["loss"]

    # %%
    pd.DataFrame(eval_metrics["class_report"])

    # %%
    alvuc.evaluation.plot_confusion_matrix(
        np.asarray(eval_metrics["confusion_matrix_normalized"]), classes=np.unique(y_pred)
    )

    # %%
    alvuc.evaluation.plot_confusion_matrix(
        np.asarray(eval_metrics["confusion_matrix_not_normalized"]),
        classes=np.unique(y_pred),
    )


if __name__ == "__main__":
    evaluation()
